<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('games')->insert([
            ['type' => 'open', 'name' => 'Single', 'rate' => 95, 'multiplication_factor' => 9.5],
            ['type' => 'open', 'name' => 'Jodi', 'rate' => 950, 'multiplication_factor' => 95],
            ['type' => 'open', 'name' => 'Single Patti', 'rate' => 1500, 'multiplication_factor' => 150 ],
            ['type' => 'open', 'name' => 'Double Patti', 'rate' => 3000, 'multiplication_factor' => 300],
            ['type' => 'open', 'name' => 'Triple Patti', 'rate' => 9000, 'multiplication_factor' => 900],
            ['type' => 'open', 'name' => 'Half Sangam', 'rate' => 12000, 'multiplication_factor' => 1200],
            ['type' => 'open', 'name' => 'Full Sangam', 'rate' => 120000, 'multiplication_factor' => 12000],
            ['type' => 'close', 'name' => 'Single', 'rate' => 95, 'multiplication_factor' => 9.5],
            ['type' => 'close', 'name' => 'Single Patti', 'rate' => 1500, 'multiplication_factor' => 150 ],
            ['type' => 'close', 'name' => 'Double Patti', 'rate' => 3000, 'multiplication_factor' => 300],
            ['type' => 'close', 'name' => 'Triple Patti', 'rate' => 9000, 'multiplication_factor' => 900],
        ]);
    }
}
