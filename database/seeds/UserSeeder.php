<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'role_id' => 1,
                'name' => 'admin',
                'user_name' => 'admin',
                'email' => 'admin@admin.com',
                'password' => Hash::make('password'),
                'mobile_no' => '8408880504',
                'location' => 'nagpur',
                'wallet_amount' => 1000,
            ],
            [
                'role_id' => 1,
                'name' => 'sumeet',
                'user_name' => 'sumeet',
                'email' => 'sumeet@admin.com',
                'password' => Hash::make('password'),
                'mobile_no' => '8408880505',
                'location' => 'nagpur',
                'wallet_amount' => 1000,
            ],
            [
                'role_id' => 1,
                'name' => 'makrand',
                'user_name' => 'makrand',
                'email' => 'makrand@admin.com',
                'password' => Hash::make('password'),
                'mobile_no' => '9766801701',
                'location' => 'nagpur',
                'wallet_amount' => 1000,
            ],
            [
                'role_id' => 3,
                'name' => 'employee',
                'user_name' => 'employee',
                'email' => 'employee@admin.com',
                'password' => Hash::make('password'),
                'mobile_no' => '0123456789',
                'location' => 'nagpur',
                'wallet_amount' => 1000,
            ],
        ]);
    }
}
