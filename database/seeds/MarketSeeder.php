<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MarketSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('markets')->insert([
            ['name' => 'SRIDEVI', 'open_time' => '11:25:00', 'close_time' => '12:25:00', 'weekly_config' => '{"Sun":1,"Mon":1,"Tue":1,"Wed":1,"Thurs":1,"Fri":1,"Sat":1}', 'sort_order' => 1],
            ['name' => 'TIME BAZAR', 'open_time' => '13:05:00', 'close_time' => '14:05:00', 'weekly_config' => '{"Sun":1,"Mon":1,"Tue":1,"Wed":1,"Thurs":1,"Fri":1,"Sat":1}', 'sort_order' => 2],
            ['name' => 'MILAN DAY', 'open_time' => '15:05:00', 'close_time' => '17:05:00', 'weekly_config' => '{"Sun":1,"Mon":1,"Tue":1,"Wed":1,"Thurs":1,"Fri":1,"Sat":1}', 'sort_order' => 3],
            ['name' => 'RAJDHANI DAY', 'open_time' => '15:25:00', 'close_time' => '17:25:00', 'weekly_config' => '{"Sun":1,"Mon":1,"Tue":1,"Wed":1,"Thurs":1,"Fri":1,"Sat":1}', 'sort_order' => 4],
            ['name' => 'KALYAN', 'open_time' => '15:50:00', 'close_time' => '17:50:00', 'weekly_config' => '{"Sun":1,"Mon":1,"Tue":1,"Wed":1,"Thurs":1,"Fri":1,"Sat":1}', 'sort_order' => 5],
            ['name' => 'SRIDEVI NIGHT', 'open_time' => '18:50:00', 'close_time' => '19:50:00', 'weekly_config' => '{"Sun":1,"Mon":1,"Tue":1,"Wed":1,"Thurs":1,"Fri":1,"Sat":1}', 'sort_order' => 6],
            ['name' => 'RAJDHANI NIGHT', 'open_time' => '21:30:00', 'close_time' => '23:35:00', 'weekly_config' => '{"Sun":1,"Mon":1,"Tue":1,"Wed":1,"Thurs":1,"Fri":1,"Sat":1}', 'sort_order' => 7],
            ['name' => 'MAIN MUMBAI RK', 'open_time' => '21:30:00', 'close_time' => '23:50:00', 'weekly_config' => '{"Sun":1,"Mon":1,"Tue":1,"Wed":1,"Thurs":1,"Fri":1,"Sat":1}', 'sort_order' => 8],
            ['name' => 'MAIN RATNA', 'open_time' => '21:30:00', 'close_time' => '23:50:00', 'weekly_config' => '{"Sun":1,"Mon":1,"Tue":1,"Wed":1,"Thurs":1,"Fri":1,"Sat":1}', 'sort_order' => 9],
            
        ]);
    }
}