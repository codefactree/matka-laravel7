<?php

use App\Model\User;
use Illuminate\Database\Seeder;

class UserRoleSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        $role = null;
        foreach (User::all() as $user) {
            switch ($user->role_id) {
                case 1:
                    $role = 'Admin';
                break;
                case 2:
                    $role = 'User';
                break;
                case 3:
                    $role = 'Employee';
                break;
                default:
                    $role = 'User';
                break;
            }
            $user->assignRole($role);
        }
    }
}
