<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('companies')->insert([
            [
                'name' => 'Sai Matka',
                'owner_name' => 'Admin',
                'mobile_no' => '9011891480',
                'how_to_play' => '{
                    "english":"English rules",
                    "hindi": "rules in Hindi
                }',
                'rates' => 'rate chart',
            ],
        ]);
    }
}
