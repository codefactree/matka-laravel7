<?php

use App\Model\User;
use Spatie\Permission\Models\Role;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $adminRole = Role::findByName('Admin');
        $employeeRole = Role::findByName('Employee');
        $adminPermissions = Permission::all();
        $adminRole->syncPermissions($adminPermissions);

        $employeePermission = Permission::whereBetween('id',[22,33])->get();
        $employeeRole->syncPermissions($employeePermission);
    }
}
