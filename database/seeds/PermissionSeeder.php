<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        Permission::create(['name' => 'user.index']);
        Permission::create(['name' => 'user.getAllUsers']);
        Permission::create(['name' => 'user.account-statement']);
        Permission::create(['name' => 'user.getAccountStatement']);
        Permission::create(['name' => 'user.show']);
        Permission::create(['name' => 'user.create']);
        Permission::create(['name' => 'user.store']);
        Permission::create(['name' => 'user.edit']);
        Permission::create(['name' => 'user.update']);
        Permission::create(['name' => 'user.destroy']);
        
        Permission::create(['name' => 'market.index']);
        Permission::create(['name' => 'market.getAllMarkets']);
        Permission::create(['name' => 'market.create']);
        Permission::create(['name' => 'market.store']);
        Permission::create(['name' => 'market.show']);
        Permission::create(['name' => 'market.edit']);
        Permission::create(['name' => 'market.update']);
        Permission::create(['name' => 'market.destroy']);
        
        Permission::create(['name' => 'wallet.addMoney']);
        Permission::create(['name' => 'getPendingWithdrawRequest']);
        Permission::create(['name' => 'withDraw.changeStatus']);
        
        Permission::create(['name' => 'result.index']);
        Permission::create(['name' => 'result.resultForMarket']);
        Permission::create(['name' => 'result.storeOpenResult']);
        Permission::create(['name' => 'result.storeCloseResult']);
        Permission::create(['name' => 'result.deleteOpenResult']);
        Permission::create(['name' => 'result.deleteCloseResult']);
        Permission::create(['name' => 'result.generateCloseResult']);
        Permission::create(['name' => 'result.userDetail']);
        Permission::create(['name' => 'forzeResult.sendMoney']);
        Permission::create(['name' => 'result.getAllResult']);
        Permission::create(['name' => 'result.details']);
        Permission::create(['name' => 'result.getDetails']);
        
        Permission::create(['name' => 'profile.edit']);
        Permission::create(['name' => 'profile.update']);
        
        Permission::create(['name' => 'company.update']);
        
        Permission::create(['name' => 'report.index']);
        Permission::create(['name' => 'report.getReport']);
        Permission::create(['name' => 'report.getTotalBidAmount']);
        
        Permission::create(['name' => 'reportDetail.index']);
        
        Permission::create(['name' => 'notification.index']);
        Permission::create(['name' => 'notification.send']);
    }
}
