<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_user_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('result_details_id');
            $table->unsignedBigInteger('user_id');
            $table->string('bidding_no');
            $table->integer('amount');
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('result_details_id')->references('id')->on('result_details')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_user_details');
    }
}
