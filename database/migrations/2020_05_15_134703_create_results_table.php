<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('results', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('market_id');
            $table->date('date');
            $table->integer('first3')->nullable();
            $table->integer('mid1')->nullable();
            $table->boolean('is_open_result_generated')->default(0);
            $table->integer('mid2')->nullable();
            $table->integer('last3')->nullable();
            $table->boolean('is_close_result_generated')->default(0);
            $table->timestamps();
            
            $table->foreign('market_id')->references('id')->on('markets')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('results');
    }
}
