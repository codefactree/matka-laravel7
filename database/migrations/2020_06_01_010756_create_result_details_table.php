<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateResultDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('result_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('result_id');
            $table->unsignedBigInteger('market_id');
            $table->unsignedBigInteger('game_id');
            $table->date('date');
            $table->integer('total_amount');
            $table->boolean('is_money_transfered')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('result_id')->references('id')->on('results');
            $table->foreign('market_id')->references('id')->on('markets');
            $table->foreign('game_id')->references('id')->on('games');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('result_details');
    }
}
