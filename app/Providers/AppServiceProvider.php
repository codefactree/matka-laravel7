<?php

namespace App\Providers;

use App\Model\User;
use App\Model\UserBid;
use App\Model\UserBidDetail;
use App\Observers\UserBidDetailObserver;
use App\Observers\UserBidObserver;
use App\Observers\UserObserver;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        User::observe(UserObserver::class);
    }
}
