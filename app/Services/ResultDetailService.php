<?php

namespace App\Services;

use App\Model\Result;
use App\Model\ResultDetail;
use App\Model\UserBid;
use App\Repository\ResultDetailRepository;
use Exception;

class ResultDetailService
{
    /**
    * ResultDetailRepository service object.
    *
    * @var ResultDetailRepository
    */
    protected $resultDetailRepository;
    
    public function __construct(ResultDetailRepository $resultDetailRepository)
    {
        $this->resultDetailRepository = $resultDetailRepository;
    }

    /**
    * Save User Bid
    *
    * @return UserBid
    * @throws Exception
    */
    public function saveResultDetail(Result $result, $data, int $total_amount)
    {
        try {
            return $this->resultDetailRepository->store([
                'result_id' => $result->id,
                'market_id' => $data->market_id,
                'game_id' => $data->game_id,
                'date' => $data->date,
                'total_amount' => $total_amount
            ]);
        } catch (Exception $exception) {
            return $exception;
        }
    }

    /**
     * Confirm that money is transfered to all user of the game
     *
     * @param ResultDetail $resultDetail
     * @return void
     */
    public function moneyIsTransfered(ResultDetail $resultDetail)
    {
        try {
            return $this->resultDetailRepository->update($resultDetail->id, [
                'is_money_transfered' => 1,
            ]);
        } catch (Exception $exception) {
            return $exception;
        }
    }
}
