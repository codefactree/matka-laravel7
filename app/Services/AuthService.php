<?php

namespace App\Services;

use App\Model\User;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class AuthService
{
    public function __construct(Client $client)
    {
        $this->client = $client;
    }
    /**
     * Send OTP to the user
     *
     * @param array $request
     * @param string $purpose
     * @return true
     * @throws Exception
     */
    public function sendOtp(array $request, string $purpose)
    {
        $otp = $this->generateOtp();
        try {
            $response = $this->sendSMS($request, $purpose, $otp);
            if($response == false) {
                return false;
            }
            Cache::put($request['mobile_no'].'_'.$purpose, $otp, now()->addMinutes(10)); // cache otp against mobile for 10 min

            return true;
        } catch (Exception $exception) {
            return $exception;
        }
    }
    
    /**
     * Validate OTP of the user 
     *
     * @param Request $request
     * @param string $purpose
     * @return true
     * @throws Exception
     */
    public function validateOtp(Request $request, string $purpose)
    {
        try {
            if (!Cache::has($request['mobile_no'].'_'.$purpose)) {
                // send error if cache is not there for the number i.e otp is expired or invalid number
                return response()->json(generic_error_response('OTP expired'));
            }
    
            if(Cache::get($request['mobile_no'].'_'.$purpose) != $request['otp']) {
                // send error if user enter invalid OTP
                return response()->json(generic_error_response('Invalid OTP'));
            }
    
            Cache::forget($request['mobile_no'].'_'.$purpose); //remove otp cache after verification
            Cache::put($request['mobile_no'] .'_'.$purpose. '_is_verified', true, now()->addMinutes(10)); // cache use verify status for registration for 10 min
            
            return true;
        } catch (Exception $exception) {
            return $exception;
        } 
    }

    /**
     * Send SMS to User
     *
     * @param array $request
     * @param string $purpose
     * @param integer $otp
     * @return boolean
     */
    private function sendSMS(array $request ,string $purpose, int $otp): bool
    {
        $message = '';
        if ($purpose == 'register') {
            $message = '%3C%23%3E Your OTP for registering at Sai application: '. $otp.' '.config('constant.sms_code');
        } else if ($purpose == 'forgetPassword') {
            $message = '%3C%23%3E Your OTP for Password change request at Sai application: '. $otp.' '.config('constant.sms_code');
        }
        $url = $this->getSMSURL($request['mobile_no'], $message);
        try {
            $response = $this->client->get($url);
            $responseBody = $response->getBody();
            if (strpos($responseBody, 'SMS-SHOOT-ID') === 0) {
                return true;
            } else {
                return false;
            }
        } catch (\Exception $exception) {
            return false;
        }
    }

   /**
    * Generate SMS URL as per the data
    *
    * @param string $mobile_no
    * @param string $message
    * @return string
    */
    private function getSMSURL(string $mobile_no, string $message): string
    {
        $config = config('constant.sms');
        return "{$config['url']}?key={$config['api_key']}&campaign={$config['campaign']}&routeid={$config['routeid']}&type=text&contacts={$mobile_no}&senderid={$config['senderid']}&msg={$message}";
    }

    /**
     * Generate OTP number
     *
     * @return int
     */
    private function generateOtp(): int
    {
        return rand(000000, 999999);
    }

    /**
     * Login user and return 
     *
     * @param User $user
     * @return string
     */
    public function loginUser(User $user): string
    {
        Auth::login($user);
        return Auth::user()->createToken('authToken')->accessToken;
    }
}
