<?php

namespace App\Services;

use App\Repository\UserBidRepository;
use Illuminate\Http\JsonResponse;

class ReportService
{
    /**
    * UserBidRepository service object.
    *
    * @var 
    */
    protected $userBidRepository;
    
    public function __construct(UserBidRepository $userBidRepository)
    {
        $this->userBidRepository = $userBidRepository;
    }

    /**
    * Get All Users from database
    *
    * @return void
    */
    public function getReport($date): JsonResponse
    {
        return $this->userBidRepository->getReport($date);
    }

    /**
    * Get Total Bid Amount from database
    *
    * @return void
    */
    public function getTotalBidAmount($date)
    {
        return $this->userBidRepository->getTotalBidAmount($date);
    }
}
