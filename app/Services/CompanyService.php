<?php

namespace App\Services;

use App\Repository\CompanyRepository;
use Exception;
use Illuminate\Http\JsonResponse;

class CompanyService
{
    /**
    * CompanyRepository service object.
    *
    * @var CompanyRepository
    */
    protected $companyRepository;
    
    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }

    /**
    * Get Company Details
    *
    * @return JsonResponse|null
    */
    public function getCompanyDetails()
    {
        try {
            return $this->companyRepository->find(1);
        } catch (Exception $exception) {
            return $exception;
        }
    }
}
