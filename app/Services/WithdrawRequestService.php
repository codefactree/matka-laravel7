<?php

namespace App\Services;

use App\Repository\WithdrawRequestRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class WithdrawRequestService
{
    /**
    * WithdrawRequestRepository service object.
    *
    * @var WithdrawRequestRepository
    */
    protected $withdrawRequestRepository;
    
    public function __construct(WithdrawRequestRepository $withdrawRequestRepository)
    {
        $this->withdrawRequestRepository = $withdrawRequestRepository;
    }

    /**
    * store Withdraw Request for logged in user
    *
    * @return JsonResponse|null
    */
    public function storeWithdrawRequest(array $data)
    {
        try {
            return $this->withdrawRequestRepository->store([
                'user_id' => Auth::user()->id,
                'amount' => $data['amount'],
                'status' => config('constant.status.pending')
            ]);
        } catch (Exception $exception) {
            return $exception;
        }
    }

    /**
     * Get withdraw list
     *
     * @return JsonResponse
     */
    public function getWithdrawList(): JsonResponse
    {
        return $this->withdrawRequestRepository->getWithdrawList();
    }

    public function getWithdrawData(string $status=null): JsonResponse
    {
        return $this->withdrawRequestRepository->getWithdrawData($status);
    }

     /**
     * Change User withdraw request status to completed
     *
     * @param integer $transactionId
     * @return bool
     */
    public function changeWithdrawStatus(int $transactionId): bool
    {
        return $this->withdrawRequestRepository->update($transactionId,[
            'status' => config('constant.status.completed'),
            'success_date' => Carbon::now()->format('Y-m-d'),
        ]);
    }
}
