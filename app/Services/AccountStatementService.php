<?php

namespace App\Services;

use App\Model\ResultUserDetail;
use App\Model\User;
use App\Model\UserBid;
use App\Model\WithdrawRequest;
use App\Repository\AccountStatementRepository;
use Exception;
use Illuminate\Support\Facades\Auth;

class AccountStatementService
{
    /**
    * AccountStatementRepository service object.
    *
    * @var AccountStatementRepository
    */
    protected $accountStatementRepository;
    
    public function __construct(AccountStatementRepository $accountStatementRepository)
    {
        $this->accountStatementRepository = $accountStatementRepository;
    }
    
    /**
    * save the winner Detail in account statement
    *
    */
    public function saveUserWinningDetail(ResultUserDetail $resultUserDetail, int $winningAmount)
    {
        try {
            return $this->accountStatementRepository->store(
                [
                'date' => getTodayDateYMD(),
                'user_id' => $resultUserDetail->user_id,
                'particulars' => $this->getParticularForBidWin($resultUserDetail, $winningAmount),
                'points' => $resultUserDetail->amount,
                'balance' => $resultUserDetail->user->wallet_amount,
                ]
            );
        } catch (Exception $exception) {
            return $exception;
        }
    }
    
    /**
    * Create Particular when user won a bid
    *
    * @param ResultUserDetail $resultUserDetail
    * @param integer $winningAmount
    * @return string
    */
    private function getParticularForBidWin(ResultUserDetail $resultUserDetail, int $winningAmount): string
    {
        $game = $resultUserDetail->resultDetail->game;
        $market = $resultUserDetail->resultDetail->market;
        return 'Won 1:'.$game->multiplication_factor. '['.$market->name.' '.$game->type.']'. 'for winning digit '. $resultUserDetail->bidding_no.', '.$winningAmount.' received';
    }
    
    /**
    * Log User Bid detail in account statement
    *
    * @param array $data
    * @param UserBid $userBid
    * @return void
    */
    public function logUserBidDetails(array $data, UserBid $userBid)
    {
        return $this->accountStatementRepository->store(
            [
                'user_id' => $userBid->user_id,
                'date' => $userBid->date,
                'particulars' => $this->getParticularsForUserBid($data, $userBid),
                'points' => $data['amount'],
                'balance' => Auth::user()->wallet_amount,
            ]
        );
    }

     /**
     * Create particular for USer biddding as per the data
     *
     * @param array $bid
     * @param UserBid $userBid
     * @return string
     */
    private function getParticularsForUserBid(array $bid, UserBid $userBid): string
    {
        return 'Played '.$userBid->market->name.' '.$userBid->game->type.' Digit '.$bid['bidding_no'];
    }

    /**
     * Log Withdraw Request in account statement
     *
     * @param WithdrawRequest $withdrawRequest
     * @param User $user
     */
    public function logWithdrawRequest(WithdrawRequest $withdrawRequest, User $user)
    {
        $date = date("Y-m-d", strtotime($withdrawRequest->created_at));
        return $this->accountStatementRepository->store(
            [
                'user_id' => $user->id,
                'date' => $date,
                'particulars' => $this->getParticularsForWithdrawRequest($withdrawRequest),
                'balance' => $user->wallet_amount,
            ]
        );
    }

    /**
     * Create particular for Withdraw request
     *
     * @param WithdrawRequest $withdrawRequest
     * @return string
     */
    private function getParticularsForWithdrawRequest(WithdrawRequest $withdrawRequest): string
    {
        return 'Withdraw requested for amount '.$withdrawRequest->amount;
    }

    /**
     * Log Add Money detail in account statement
     *
     * @param array $data
     * @param User $user
     * @return void
     */
    public function addMoneyToWalletByAdmin(array $data, User $user)
    {
        $date = date("Y-m-d", strtotime($user->updated_at));

        return $this->accountStatementRepository->store(
            [
                'user_id' => $user->id,
                'date' => $date,
                'particulars' => $this->getParticularsForAddMoneyByAdmin($data, $user),
                'balance' => $user->wallet_amount,
            ]
        );
    }

    /**
     * Create particular for Add Money by Admin
     *
     * @param array $data
     * @param User $user
     * @return string
     */
    private function getParticularsForAddMoneyByAdmin(array $data, User $user): string
    {
        return $data['amount'].' Points added by Admin';
    }

    /**
     * Get Account statement
     *
     * @param User $user
     * @return void
     */
    public function getAccountStatement(User $user)
    {
        return $this->accountStatementRepository->getAccountStatement($user);
    }
    
    /**
     * Get Account statement
     *
     * @param User $user
     * @return void
     */
    public function getAccountStatementForDatable(User $user)
    {
        return $this->accountStatementRepository->getAccountStatementForDatable($user);
    }
}
