<?php

namespace App\Services;

use App\Model\Market;
use App\Model\Result;
use App\Repository\MarketRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

class MarketService
{
    /**
    * MarketRepository service object.
    *
    * @var MarketRepository
    */
    protected $marketRepository;
    
    public function __construct(MarketRepository $marketRepository)
    {
        $this->marketRepository = $marketRepository;
    }
    
    /**
    * Get All Market from database
    *
    * @return JsonResponse
    */
    public function getAllMarkets(): JsonResponse
    {
        return $this->marketRepository->getAllMarkets();
    }
    
    /**
    * Get All Market for REsult page from database
    *
    * @return JsonResponse
    */
    public function getAllMarketForResult(string $date): JsonResponse
    {
        return $this->marketRepository->getAllMarketForResult($date);
    }
    
    /**
    * Get All Market for reorder
    *
    * @return Collection
    */
    public function getMarketLists(): Collection
    {
        return $this->marketRepository->getMarketLists();
    }
    
    /**
    * Update reorder 
    *
    * @return Collection
    */
    public function updateReorder(array $data)
    {
        unset($data['_token']);
        unset( $data['_method']);
        try {
            foreach($data as $marketKey => $marketValue) {
                Market::where('id', $marketKey)
                ->update(['sort_order' => $marketValue]);
            }
        } catch (Exception $exception) {
            return $exception;
        }
        return true;
    }
    
    /**
    * Save User in database
    *
    * @return JsonResponse
    */
    public function saveMarket(array $data)
    {
        return $this->marketRepository->store([
            'name' => $data['name'],
            'open_time' => date("H:i", strtotime($data['openTimePicker'])),
            'close_time' => date("H:i", strtotime($data['closeTimePicker'])),
            'weekly_config' => json_encode($this->getWeekConfig($data['days'])),
            ]
        );
    }
    
    /**
    * Update User in database
    *
    * @param array $data
    * @param Market $market
    * @return boolean
    */
    public function updateMarket(array $data, Market $market): bool
    {
        $attributes = [
            'name' => $data['nameEdit'],
            'open_time' => date("H:i", strtotime($data['openTimePickerEdit'])),
            'close_time' => date("H:i", strtotime($data['closeTimePickerEdit'])),
            'weekly_config' => json_encode($this->getWeekConfig($data['days'] ?? [])),
        ];
        
        return $this->marketRepository->update($market->id, $attributes);
    }
    
    /**
    * Create Weekly config array as per the user selected days
    *
    * @param array $selectedDays
    * @return array
    */
    private function getWeekConfig(array $selectedDays): array
    {
        $weekConfig = null;
        if(empty($selectedDays)) {
            foreach(config('constant.week') as $dayName) {
                $weekConfig[$dayName] = 0;
            }
        } else {
            foreach(config('constant.week') as $dayName) {
                if(in_array($dayName, $selectedDays)) {
                    $weekConfig[$dayName] = 1;
                } else {
                    $weekConfig[$dayName] = 0;
                    
                }
            }
        }
        
        return $weekConfig;
    }
    
    /**
    * Delete User from database
    *
    * @return bool
    */
    public function deleteMarket(int $id): bool
    {
        return $this->marketRepository->delete($id);
    }
    
    /**
    * 
    *  API SECTION
    * 
    */
    
    /**
    * Api to get All Market Details for Mobile App Dashboard
    *
    * @return JsonResponse|null
    */
    public function getAllMarketDetailsForApi()
    {
        try {
            $markets = $this->marketRepository->getMarketLists();
            foreach($markets as $market) {
                $marketResult = $market->result(getTodayDateYMD())->first();
                if($marketResult) {
                    $this->fillResult($market, $marketResult);
                } else {
                    $timeDifference = Carbon::parse($market->open_time)->diffInMinutes(Carbon::now(), false);
                    if($timeDifference > -240) { //if time difference is less then hour show *
                        $market->first3 = '***';
                        $market->mid1 = '*';
                        $market->mid2 = '*';
                        $market->last3 = '***';
                    } else {
                        $prevDate = date('Y-m-d', strtotime("-1 days"));
                        $marketYesterdatResult = $market->result($prevDate)->first();
                       $this->fillResult($market, $marketYesterdatResult);
                    }
                }
                $market->open_time = date("h:i a", strtotime($market->open_time)); //convert to 12 hour format
                $market->close_time = date("h:i a", strtotime($market->close_time)); //convert to 12 hour format
            }

            return $markets;
        } catch (Exception $exception) {
            return $exception;
        }
    }
    
    /**
     * Fill the respective market with the result
     *
     * @param Market $market
     * @param Result $marketResult
     * @return Market
     */
    private function fillResult(Market $market, Result $marketResult = null): Market
    {
        $market->first3 = $marketResult->first3 ?? '***';
        $market->mid1 = $marketResult->mid1 ?? '*';
        $market->mid2 = $marketResult->mid2 ?? '*';
        $market->last3 = $marketResult->last3 ?? '***';

        return $market;
    }
}
