<?php

namespace App\Services;

use App\Repository\GameRepository;

class GameService
{
    /**
    * GameRepository service object.
    *
    * @var GameRepository
    */
    protected $gameRepository;
    
    public function __construct(GameRepository $gameRepository)
    {
        $this->gameRepository = $gameRepository;
    }
    
    /**
    * Get Company Details
    *
    * @return JsonResponse|null
    */
    public function getAllGames()
    {
        return $this->gameRepository->all();
    }
}
