<?php

namespace App\Services;

use App\Model\User;
use App\Model\UserBid;
use App\Repository\UserBidRepository;
use Exception;

class UserBidService
{
    /**
    * UserBidRepository service object.
    *
    * @var UserBidRepository
    */
    protected $userBidRepository;
    
    public function __construct(UserBidRepository $userBidRepository)
    {
        $this->userBidRepository = $userBidRepository;
    }

    /**
    * Save User Bid
    *
    * @return UserBid
    * @throws Exception
    */
    public function saveUserBid(array $data, User $user)
    {
        try {
            return $this->userBidRepository->store([
                'user_id' => $user->id,
                'game_id' => $data['game_id'],
                'game_type' => $data['game_id'] <= 7 ? 'open' : 'close',
                'market_id' => $data['market_id'],
                'date' => $data['date'],
                'total_amount' => $data['total_amount'],
            ]);
        } catch (Exception $exception) {
            return $exception;
        }
    }
}
