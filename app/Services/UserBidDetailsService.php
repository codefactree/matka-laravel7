<?php

namespace App\Services;

use App\Model\UserBid;
use App\Repository\UserBidDetailsRepository;
use Exception;

class UserBidDetailsService
{
    /**
    * UserBidDetailsRepository service object.
    *
    * @var UserBidDetailsRepository
    */
    protected $userBidDetailsRepository;
    
    public function __construct(UserBidDetailsRepository $userBidDetailsRepository)
    {
        $this->userBidDetailsRepository = $userBidDetailsRepository;
    }

    /**
    * Get Company Details
    *
    * @return UserBidDetails
    * @throws Exception
    */
    public function saveUserBidDetails(array $data, UserBid $userBid)
    {
        return $this->userBidDetailsRepository->store([
            'user_bid_id' => $userBid->id,
            'user_id' => $userBid->user_id,
            'bidding_no' => $data['bidding_no'],
            'amount' => $data['amount'],
        ]);
    }
}
