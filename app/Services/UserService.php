<?php

namespace App\Services;

use App\Model\User;
use App\Repository\UserDetailRepository;
use App\Repository\UserRepository;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserService
{
    /**
    * UserRepository service object.
    *
    * @var UserRepository
    */
    protected $userRepository;
    
    /**
    * UserDetailRepository service object.
    *
    * @var UserDetailRepository
    */
    protected $userDetailRepository;
    
    public function __construct(
        UserRepository $userRepository,
        UserDetailRepository $userDetailRepository
    ) {
        $this->userRepository = $userRepository;
        $this->userDetailRepository = $userDetailRepository;
    }
    
    /**
    * Get All Users from database
    *
    * @return JsonResponse
    */
    public function getAllUsers(): JsonResponse
    {
        return $this->userRepository->getAllUsers();
    }
    
    /**
    * Save User in database
    *
    * @return bool
    * @throws Exception
    */
    public function saveUser(array $data)
    {
        try{
            DB::beginTransaction();
            $user = $this->userRepository->store($this->getUserDataArray($data));
            $this->userDetailRepository->store($this->getUserDetailDataArray($data, $user));
            $roleAssigned = $this->assignRoleToUser($user);
            if($roleAssigned instanceof Exception) {
                DB::rollBack();
            
                return $roleAssigned;
            }

            DB::commit();
            
            return $user;
        } catch(Exception $exception) {
            DB::rollBack();
            
            return $exception;
        }
    }

    /**
     * Assign Role to User
     *
     * @param User $user
     * @return void
     */
    private function assignRoleToUser(User $user)
    {
        switch ($user->role_id) {
            case 1:
                $role = 'Admin';
            break;
            case 2:
                $role = 'User';
            break;
            case 3:
                $role = 'Employee';
            break;
            default:
                $role = 'User';
            break;
        }
        try {
            return $user->assignRole($role);
        } catch (Exception $exception) {
            return $exception;
        }
    }
    
    /**
    * Update User in database
    *
    * @param array $data
    * @param User $user
    * @return boolean
    * @throws Exception
    */
    public function updateUser(array $data, User $user)
    {
        try {
            DB::beginTransaction();
            $this->userRepository->update($user->id, $this->getUserDataArray($data));
            $this->userDetailRepository->updateWhere(
                [
                    'user_id' => $user->id
                ], 
                $this->getUserDetailDataArray($data)
            );
            DB::commit();
            
            return true;
        } catch (Exception $exception) {
            DB::rollBack();
            
            return $exception;
        }
    }

    /**
    * Update User Token in database
    *
    * @param array $data
    * @param User $user
    * @return boolean
    * @throws Exception
    */
    public function saveToken(array $data, User $user)
    {
        try {
            DB::beginTransaction();
            $this->userRepository->update($user->id, [
                'fcm_token' => $data['fcm_token']
            ]);
            DB::commit();
            
            return true;
        } catch (Exception $exception) {
            DB::rollBack();
            
            return $exception;
        }
    }
    
    /**
    * format User data as per the requirement
    *
    * @param array $data
    * @return array
    */
    private function getUserDataArray(array $data): array
    {
        $attributes =  [
            'name' => $data['name'],
            'email' => $data['email'] ?? null,
            'user_name' => $data['user_name'],
            'mobile_no' => $data['mobile_no'],
            'location' => $data['location'] ?? null,
        ];

        if(isset($data['password'])) {
            $attributes['password'] = Hash::make($data['password']);
        }

        if(isset($data['role'])) {
            $attributes['role_id'] = $data['role'];
        }

        if(isset($data['wallet_amount'])) {
            $attributes['wallet_amount'] = $data['wallet_amount'];
        }
        
        return $attributes;
    }
    
    /**
    * format User Detail data as per the requirement
    *
    * @param array $data
    * @param User $user
    * @return array
    */
    private function getUserDetailDataArray(array $data, User $user=null): array
    {
        $attributes = [
            'phonePe' => $data['phonePe'] ?? null,
            'googlepay' => $data['googlepay'] ?? null,
            'paytm' => $data['paytm'] ?? null,
            'bank_name' => $data['bank_name'] ?? null,
            'branch_name' => $data['branch_name'] ?? null,
            'account_no' => $data['account_no'] ?? null,
            'ifsc_code' => $data['ifsc_code'] ?? null,
        ];
        if (!is_null($user)) {
            $attributes['user_id'] = $user->id;
        }
        
        return $attributes;
    }
    
    /**
    * Delete User from database
    *
    * @return bool
    */
    public function deleteUser(int $id): bool
    {
        return $this->userRepository->delete($id);
    }
    
    /**
    * Debit amount from Wallet 
    *
    * @return void
    */
    public function debitWaletAmount(array $data, User $user)
    {
        try {
            $user->wallet_amount =  $user->wallet_amount - $data['amount'];
            $user->save();
        } catch (Exception $exception) {
            return $exception;
        }
    }

    /**
    * Add amount in Wallet 
    *
    * @return void
    */
    public function addMoneyToWallet(array $data, User $user)
    {
        try {
            return $this->userRepository->update($user->id, [
                'wallet_amount' => $user->wallet_amount + $data['amount']
                ]
            );
        } catch (Exception $exception) {
            return $exception;
        }
    }

   /**
    * Find User from mobile number 
    *
    * @param array $data
    * @return User
    * @throws Exception
    */
    public function findUser(array $data)
    {
        try {
            return $this->userRepository->findWhere([
                'mobile_no' => $data['mobile_no']
            ]);
        } catch (Exception $exception) {
            return $exception;
        }
    }

    /**
    * Reset Password for given user
    *
    * @return void
    */
    public function resetPassword(array $data, User $user)
    {
        try {
            return $this->userRepository->update($user->id, [
                'password' => Hash::make($data['password'])
            ]);
        } catch (Exception $exception) {
            return $exception;
        }
    }

    /**
     * Clear FCM token when logout
     *
     * @param User $user
     * @return boolean
     */
    public function clearFCMToken(User $user): bool
    {
        try {
            return $this->userRepository->update($user->id, [
                'fcm_token' => null
            ]);
        } catch (Exception $exception) {
            return $exception;
        }
    }
}
