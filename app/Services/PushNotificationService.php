<?php

namespace App\Services;

use App\Model\Result;
use App\Model\User;
use Illuminate\Http\JsonResponse;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class PushNotificationService
{
    /**
    * Send Result Notification to all users
    *
    * @return JsonResponse|null
    */
    public function sendResultNotification(Result $result, string $mode)
    {
        if($mode == 'open') {
            $number =  $result->first3.'-'.$result->mid1.'*-***';
        } else {
            $number = $result->first3.'-'.$result->mid1.$result->mid2. '-'.$result->last3;
        }
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);
        $optionBuilder->setPriority("high");
        $optionBuilder->setContentAvailable(true);

        $notificationBuilder = new PayloadNotificationBuilder( $result->market->name.' ' .$mode);
        $notificationBuilder->setBody($number)
        ->setSound('default');
        
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData([
            'type' => 'result',
            'title' =>  $result->market->name.' ' .$mode,
            'message' => $number,
        ]);
        
        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        
        // You must change it to get your tokens
        $tokens = User::pluck('fcm_token')->toArray();
        
        $downstreamResponse = FCM::sendTo($tokens, $option, null, $data);
        
        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
        
        // return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();
        
        // return Array (key : oldToken, value : new token - you must change the token in your database)
        $downstreamResponse->tokensToModify();
        
        // return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();
        
        // return Array (key:token, value:error) - in production you should remove from your database the tokens present in this array
        $downstreamResponse->tokensWithError();
    }

    /**
    * Send custom push notification 
    *
    * @return JsonResponse|null
    */
    public function sendCustomNotification(array $data)
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);
        $optionBuilder->setPriority("high");
        $optionBuilder->setContentAvailable(true);
        
        $notificationBuilder = new PayloadNotificationBuilder($data['title']);
        $notificationBuilder->setBody($data['body'])
        ->setSound('default');
        
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData([
            'type' => 'custom',
            'title' => $data['title'],
            'message' => $data['body'],
        ]);
        
        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        // You must change it to get your tokens
        $tokens = User::pluck('fcm_token')->toArray();
        
        $downstreamResponse = FCM::sendTo($tokens, $option, null, $data);
        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
        
        // return Array - you must remove all this tokens in your database
        $downstreamResponse->tokensToDelete();
        
        // return Array (key : oldToken, value : new token - you must change the token in your database)
        $downstreamResponse->tokensToModify();
        
        // return Array - you should try to resend the message to the tokens in the array
        $downstreamResponse->tokensToRetry();
        
        // return Array (key:token, value:error) - in production you should remove from your database the tokens present in this array
        $downstreamResponse->tokensWithError();
    }
}
