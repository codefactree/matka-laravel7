<?php

namespace App\Services;

use App\Model\Market;
use App\Repository\UserBidDetailsRepository;
use Exception;
use Illuminate\Support\Collection;

class ReportDetailService
{
    /**
    * UserBidRepository service object.
    *
    * @var 
    */
    protected $userBidDetailsRepository;
    
    public function __construct(UserBidDetailsRepository $userBidDetailsRepository)
    {
        $this->userBidDetailsRepository = $userBidDetailsRepository;
    }
    
    /**
    * Get Report Detail i.e. amount on the number as per the game
    *
    * @param Market $market
    * @param string $gameType
    * @param string $date
    * @return Collection
    * @throws Exception
    */
    public function getDetailReport(Market $market, string $gameType, string $date)
    {
        $result = $this->userBidDetailsRepository->getDetailReport($market, $gameType, $date);
        if($result instanceof Exception) {
            return $result;
        }
        return $this->formatReportDetailResult($result);
    }

    /**
    * Get Report Detail i.e. amount on the number as per the game
    *
    * @param Market $market
    * @param string $gameType
    * @param string $date
    * @return Collection
    * @throws Exception
    */
    public function getEachGameTotal(Market $market, string $gameType, string $date)
    {
        $result = $this->userBidDetailsRepository->getEachGameTotal($market, $gameType, $date);
        if($result instanceof Exception) {
            return $result;
        }

        return $this->formatReportDetailResult($result);
    }


    
    /**
    * Format Result as per the required format
    *
    * @param Collection $result
    * @return array
    */
    private function formatReportDetailResult(Collection $result): array
    {
        $gameWiseResult=null;
        foreach ($result as  $value) {
            $gameWiseResult[$value->game_id][]= $value;
        }
        
        $formattedData=null;
        $row = 1;
        foreach($gameWiseResult as $gameId => $gameData) {
            foreach ($gameData as $key => $value) {
                $formattedData[$row][$gameId] = $value;
                $row++;
            }
            $row = 1;
        }
        return $formattedData;
    }
}
