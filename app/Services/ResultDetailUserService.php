<?php

namespace App\Services;

use App\Model\ResultDetail;
use App\Model\UserBid;
use App\Repository\ResultUserDetailRepository;
use Exception;

class ResultDetailUserService
{
    /**
    * ResultUserDetailRepository service object.
    *
    * @var ResultUserDetailRepository
    */
    protected $resultUserDetailRepository;
    
    public function __construct(ResultUserDetailRepository $resultUserDetailRepository)
    {
        $this->resultUserDetailRepository = $resultUserDetailRepository;
    }

    /**
    * Save User Bid
    *
    * @return UserBid
    * @throws Exception
    */
    public function saveResultDetailUser($data, ResultDetail $resultDetail)
    {
        try {
            return $this->resultUserDetailRepository->store([
                'result_details_id' => $resultDetail->id,
                'user_id' => $data->user_id,
                'bidding_no' => $data->bidding_no,
                'amount' => $data->amount
            ]);
        } catch (Exception $exception) {
            return $exception;
        }
    }
}
