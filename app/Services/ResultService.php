<?php

namespace App\Services;

use App\Model\Market;
use App\Model\Result;
use App\Repository\ResultRepository;
use App\Repository\UserBidRepository;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\JsonResponse;

class ResultService
{
    /**
    * ResultRepository service object.
    *
    * @var ResultRepository
    */
    protected $resultRepository;
    /**
    * UserBidRepository service object.
    *
    * @var UserBidRepository
    */
    protected $userBidRepository;
    
    public function __construct(
        ResultRepository $resultRepository, 
        UserBidRepository $userBidRepository
    ) {
        $this->resultRepository = $resultRepository;
        $this->userBidRepository = $userBidRepository;
    }
    
    /**
     * Find Result as per Close market data
     * Check if open Result is already generated or not 
     *
     * @param array $data
     * @param String $mode
     */
    public function findResultAsPerCloseMarketData(array $data, String $mode)
    {
        return $this->resultRepository->findWhere([
            'market_id' => $data['marketIdClose'],
            'date' => convertDMYtoYMD($data['dateClose'])
        ]);
    }
    
    /**
    * Store Result
    *
    * @return Result
    */
    public function storeResult(array $data, String $mode): Result
    {
        return $this->resultRepository->storeResult($data, $mode);
    }

    /**
    * Delete Open/Close result as per mode
    * Make the result field of the selected market Blank as per the mode
    *
    * @param Market $market
    * @return boolean
    */
    public function deleteOpenResult(Market $market, String $mode): bool
    {
        $threeDigitField = '';
        $oneDigitField = '';

        if($mode == 'open') {
            $threeDigitField = 'first3';
            $oneDigitField = 'mid1';
        } else {
            $threeDigitField = 'last3';
            $oneDigitField = 'mid2';
        }

        return $this->resultRepository->updateWhere([
            'market_id' => $market->id,
            'date' => Carbon::now()->format('Y-m-d'),
        ], [
            $threeDigitField => null,
            $oneDigitField => null
        ]);
    }

    /**
    * Get Result Details for the market
    *
    * @return JsonResponse
    */
    public function getResultDetailsForMarket(Market $market): JsonResponse
    {
        return $this->resultRepository->getResultDetailsForMarket($market);
    }

    /**
    * Generate total amount for Market
    *
    * @param Market $market
    * @return JsonResponse
    */
    public function getTotalAmountForMarket(Market $market, string $marketType, string $date): array
    {
        return $this->userBidRepository->getTotalAmountForMarket($market, $marketType, $date);
    }
    
    /**
    * get total bid amount for market for report page
    *
    * @param Market $market
    * @return JsonResponse
    */
    public function getTotalMarketAmountForReport(Market $market, string $marketType, string $date): array
    {
        try {
            return $this->userBidRepository->getTotalMarketAmountForReport($market, $marketType, $date);
        } catch(Exception $exception){
            return $exception;
        }
    }

    /**
     * Generate Result;
     *
     * @param Market $market
     */
    public function generateOverallResult(Market $market, int $gameId, $biddingNo, string $date)
    {
        return $this->userBidRepository->calculateOverallGameResult($market, $gameId, $biddingNo, $date);
    }

    /**
     * Generate Overall ResultResult;
     *
     * @param Market $market
     */
    public function generateOverallHalfSangamResult(Market $market, int $gameId, Result $result, string $date)
    {
        return $this->userBidRepository->calculateOverallHalfSangamGameResult($market, $gameId, $result, $date);
    }

    /**
     * Generate Result User wise i.e showing which user has bidded how much amount and how much he is earning;
     *
     * @param Market $market
     */
    public function generateResultUserwise(Market $market, int $gameId, $biddingNo, string $date)
    {
        return $this->userBidRepository->calculateGameResultUserwise($market, $gameId, $biddingNo, $date);
    }
    

    /**
     * Generate Result User wise i.e showing which user has bidded how much amount and how much he is earning;
     *
     * @param Market $market
     */
    public function generateHalfSangamResultUserwise(Market $market, int $gameId, Result $result, string $date)
    {
        return $this->userBidRepository->calculateHalfSangamGameResultUserwise($market, $gameId, $result, $date);
    }

     /**
     * Generate Resul Total amount for that game;
     *
     * @param Market $market
     */
    public function generateTotalAmountResultUserwise(Market $market, int $gameId, $biddingNo, string $date)
    {
        return $this->userBidRepository->calculateGameResultTotalUserwise($market, $gameId, $biddingNo, $date);
    }

    /**
     * Generate Result;
     *
     * @param Market $market
     */
    public function generateResultToFreeze(Market $market, int $gameId, $biddingNo, string $date)
    {
        return $this->userBidRepository->calculateGameResultToFreeze($market, $gameId, $biddingNo, $date);
    }

    
    /**
     * Generate Result;
     *
     * @param Result $result
     */
    public function resultIsGenerated(Result $result, string $gameType)
    {
        if ($gameType == 'open') {
            $resultGeneratedField = 'is_open_result_generated';
        } else {
            $resultGeneratedField = 'is_close_result_generated';
        }
        return $this->resultRepository->update($result->id,[
            $resultGeneratedField => 1
        ]);
    }
}
