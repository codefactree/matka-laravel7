<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class TodayTimeValidation implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(array $request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $todaysDate = Carbon::now()->format('d-m-Y');
        $currentTime = Carbon::now()->format('h:i a');
        if ($todaysDate != $this->request['dateOpen']) {
            return true; // if its not today's date, then no need to check for time validation
        }
        return  $currentTime < $value; // check if time is passed or not; 
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Seems like you are trying to enter value for today's time which is already passed.";
    }
}
