<?php

namespace App\Repository;

use App\Model\ResultDetail;

class ResultDetailRepository extends Repository
{
	/**
     * ResultDetail model object
     * @var User
     */
	protected $model;
	
	public function __construct(ResultDetail $model)
	{
		$this->model = $model;
	}
}
