<?php

namespace App\Repository;

use App\Model\UserDetail;

class UserDetailRepository extends Repository
{
	/**
     * UserDetail model object
     * @var User
     */
	protected $model;
	
	public function __construct(UserDetail $model)
	{
		$this->model = $model;
	}
}
