<?php

namespace App\Repository;

use App\Model\Market;
use App\Model\Result;
use App\Model\UserBid;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class UserBidRepository extends Repository
{
    /**
    * UserBid model object
    * @var UserBid
    */
    protected $model;
    
    public function __construct(UserBid $model, DataTables $dataTables)
    {
        $this->model = $model;
        $this->dataTables = $dataTables;
    }
    
    /**
    * Get Reports from database for Data table
    *
    * @return JsonResponse
    */
    public function getReport(string $date): JsonResponse
    {
        $query = DB::table('user_bids')
        ->leftJoin('markets', 'user_bids.market_id', '=', 'markets.id')
        ->select('market_id','game_type', 'name', DB::raw('sum(total_amount) as total'))
        ->groupBy('market_id','game_type')
        ->orderBy('name')
        ->where('date', $date);
        
        return $this->dataTables->queryBuilder($query)
        ->addColumn('action', function ($market) use ($date) {
            $date =  date("d-m-yy", strtotime($date));
            return '
            <div>
            <a id="detailModalButton" href="/admin/report/market/'.$market->market_id.'?date='.$date.'&game-type='.$market->game_type.'" class="btn btn-outline-success btn-sm"><i class="fas fa-eye"></i></a>
            </div>';
        })
        ->rawColumns(['action'])
        ->addIndexColumn()
        ->make(true);
    }
    
    /**
    * Get Total Bid Amount
    *
    * @param string $date
    * @return string
    */
    public function getTotalBidAmount(string $date)
    {
        return $this->model->where('date', $date)->sum('total_amount');
    }
    
    /**
    * Get Total Bid Amount
    *
    * @param string $date
    * @return array
    */
    public function getTotalAmountForMarket(Market $market, string $marketType, string $date): array
    {
        if ($marketType == 'open') {
            $gameIds = config('constant.game_for_open_result');
        } elseif ($marketType == 'close') {
            $gameIds = config('constant.game_for_close_result');
        }
        $result['total_bidding_amount'] = $this->model
        ->where('game_type', $marketType)
        ->where('market_id', $market->id)
        ->whereIn('game_id', $gameIds)
        ->where('date', $date)
        ->sum('total_amount');
        
        return $result;
    }
    
    /**
    * Get Total Bid Amount for Market for Report as per game type
    *
    * @param string $date
    * @return array
    */
    public function getTotalMarketAmountForReport(Market $market, string $marketType, string $date): array
    {
        if ($marketType == 'open') {
            $gameIds = [1,2,3,4,5,6,7];
        } elseif ($marketType == 'close') {
            $gameIds = [8,9,10,11];
        }
        $result['total_bidding_amount'] = $this->model
        ->where('game_type', $marketType)
        ->where('market_id', $market->id)
        ->whereIn('game_id', $gameIds)
        ->where('date', $date)
        ->sum('total_amount');
        
        return $result;
    }
    
    /**
    * Calculate Overall result 
    * i.e how much we need to give on the result number generated 
    * to be displayed on result page
    *
    * @param Market $market
    */
    public function calculateOverallGameResult(Market $market, int $gameId, $biddingNo, string $date)
    {
        return DB::table('user_bids')
        ->leftJoin('user_bid_details', 'user_bids.id', '=', 'user_bid_details.user_bid_id')
        ->where('date', $date)
        ->where('game_id', $gameId)
        ->where('market_id', $market->id)
        ->where('bidding_no', $biddingNo)
        ->select('game_id', DB::raw('sum(amount) as total'))
        ->first();
    }

    /**
    * Calculate Overall result for Half Sangam
    * i.e how much we need to give on the result number generated 
    * to be displayed on result page
    *
    * @param Market $market
    */
    public function calculateOverallHalfSangamGameResult(Market $market, int $gameId, Result $result, $date)
    {
        return DB::table('user_bids')
        ->leftJoin('user_bid_details', 'user_bids.id', '=', 'user_bid_details.user_bid_id')
        ->where('date', $date)
        ->where('game_id', $gameId)
        ->where('market_id', $market->id)
        ->whereIn('bidding_no', [$result->mid1.'--'.$result->last3, $result->first3.'--'.$result->mid2])
        ->select('game_id', DB::raw('sum(amount) as total'))
        ->first();
    }
    
    /**
    * Calculate Resutl User wise i.e showing which user has bidded how much amount and how much he is earning
    *
    * @param Market $market
    */
    public function calculateGameResultUserwise(Market $market, int $gameId, $biddingNo, string $date)
    {
        if($gameId == 6) {
            return DB::table('user_bids')
            ->leftJoin('user_bid_details', 'user_bids.id', '=', 'user_bid_details.user_bid_id')
            ->leftJoin('users', 'user_bids.user_id', '=', 'users.id')
            ->where('date', $date)
            ->where('game_id', $gameId)
            ->where('market_id', $market->id)
            ->whereIn('bidding_no', $biddingNo)
            ->select('name','email', 'user_name','mobile_no', 'bidding_no', 'amount')
            ->get();
        } else {
            return DB::table('user_bids')
            ->leftJoin('user_bid_details', 'user_bids.id', '=', 'user_bid_details.user_bid_id')
            ->leftJoin('users', 'user_bids.user_id', '=', 'users.id')
            ->where('date', $date)
            ->where('game_id', $gameId)
            ->where('market_id', $market->id)
            ->where('bidding_no', $biddingNo)
            ->select('name','email', 'user_name','mobile_no', 'bidding_no', 'amount')
            ->get();
        }
    }
   
    /**
    * Calculate Result User wise i.e showing which user has bidded how much amount and how much user is earning
    *
    * @param Market $market
    */
    public function calculateHalfSangamGameResultUserwise(Market $market, int $gameId, Result $result, $date)
    {
        return DB::table('user_bids')
        ->leftJoin('user_bid_details', 'user_bids.id', '=', 'user_bid_details.user_bid_id')
        ->leftJoin('users', 'user_bids.user_id', '=', 'users.id')
        ->where('date', $date)
        ->where('game_id', $gameId)
        ->where('market_id', $market->id)
        ->whereIn('bidding_no', [$result->mid1.'--'.$result->last3, $result->first3.'--'.$result->mid2])
        ->select('name','email', 'user_name','mobile_no', 'bidding_no', 'amount')
        ->get();
    }

    /**
    * Calculate Single Bid repository
    *
    * @param Market $market
    */
    public function calculateGameResultTotalUserwise(Market $market, int $gameId, $biddingNo, string $date)
    {
        if($gameId == 6) {
            return DB::table('user_bids')
            ->leftJoin('user_bid_details', 'user_bids.id', '=', 'user_bid_details.user_bid_id')
            ->leftJoin('users', 'user_bids.user_id', '=', 'users.id')
            ->where('date', $date)
            ->where('game_id', $gameId)
            ->where('market_id', $market->id)
            ->whereIn('bidding_no', $biddingNo)
            ->select(DB::raw('sum(amount) as total_amount'))
            ->first();
        } else {
            return DB::table('user_bids')
            ->leftJoin('user_bid_details', 'user_bids.id', '=', 'user_bid_details.user_bid_id')
            ->leftJoin('users', 'user_bids.user_id', '=', 'users.id')
            ->where('date', $date)
            ->where('game_id', $gameId)
            ->where('market_id', $market->id)
            ->where('bidding_no', $biddingNo)
            ->select(DB::raw('sum(amount) as total_amount'))
            ->first();
        }
    }

    /**
    * Calculate Game Result to freeze
    *
    * @param Market $market
    */
    public function calculateGameResultToFreeze(Market $market, int $gameId, $biddingNo, string $date)
    {
        if($gameId == 6) {
            $biddingNo = [
                $biddingNo->first3.'--'.$biddingNo->mid2,
                $biddingNo->mid1.'--'.$biddingNo->last3,
            ];
            return DB::table('user_bids')
                ->leftJoin('user_bid_details', 'user_bids.id', '=', 'user_bid_details.user_bid_id')
                ->where('date', $date)
                ->where('game_id', $gameId)
                ->where('market_id', $market->id)
                ->whereIn('bidding_no', $biddingNo)
                ->select('market_id','game_id','date','user_bids.user_id','bidding_no', 'amount')
                ->get();
        } else {
            return DB::table('user_bids')
                ->leftJoin('user_bid_details', 'user_bids.id', '=', 'user_bid_details.user_bid_id')
                ->where('date', $date)
                ->where('game_id', $gameId)
                ->where('market_id', $market->id)
                ->where('bidding_no', $biddingNo)
                ->select('market_id','game_id','date','user_bids.user_id','bidding_no', 'amount')
                ->get();
        }
    }
}
