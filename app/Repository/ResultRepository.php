<?php

namespace App\Repository;

use App\Model\Market;
use App\Model\Result;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Yajra\DataTables\DataTables;

class ResultRepository extends Repository
{
    /**
    * Result model object
    * @var Result
    */
    protected $model;
    
    public function __construct(Result $model)
    {
        $this->model = $model;
    }
    
    /**
    * Store Result
    *
    * @return void
    */
    public function storeResult(array $data, String $mode): Result
    {
        if($mode === 'open') {
            $marketId = 'marketIdOpen';
            $dateField = 'dateOpen';
            $number = 'first3';
            $mid_number = 'mid1';
        } else {
            $marketId = 'marketIdClose';
            $dateField = 'dateClose';
            $number = 'last3';
            $mid_number = 'mid2';
        }

        return $this->model->updateOrCreate(
            ['market_id' => $data[$marketId], 'date' => Carbon::parse($data[$dateField])->format('Y-m-d')],
            [$number =>  $data[$number], $mid_number => $data[$mid_number]]
        );
    }
    
    /**
    * Get Result Details for the market
    *
    * @return array
    */
    public function getResultDetailsForMarket(Market $market): JsonResponse
    {
        return DataTables::of($this->model->query()->where('market_id', $market->id)->latest('date'))
        ->addColumn('number', function ($result) {
            $first3 = $result->first3 != null ? $result->first3 : '***'; 
            $mid1 = $result->mid1 != null ? $result->mid1 : '*'; 
            $mid2 = $result->mid2 != null ? $result->mid2 : '*'; 
            $last3 = $result->last3 != null ? $result->last3 : '***'; 
            return $first3.'-'.$mid1.$mid2.'-'.$last3;
        })
        ->editColumn('date', function($data) {
            return date("d-m-Y", strtotime($data->date));
        })
        ->addIndexColumn()
        ->make(true);
    }
}
