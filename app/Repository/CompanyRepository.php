<?php

namespace App\Repository;

use App\Model\Company;

class CompanyRepository extends Repository
{
	/**
     * Company model object
     * @var Company
     */
	protected $model;
	
	public function __construct(Company $model)
	{
		$this->model = $model;
	}
}
