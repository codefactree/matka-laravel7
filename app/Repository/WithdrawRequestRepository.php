<?php

namespace App\Repository;

use App\Model\WithdrawRequest;
use Illuminate\Http\JsonResponse;
use Yajra\DataTables\DataTables;

class WithdrawRequestRepository extends Repository
{
    /**
    * UserTransaction model object
    * @var UserTransaction
    */
    protected $model;
    
    public function __construct(WithdrawRequest $model, DataTables $dataTables)
    {
        $this->model = $model;
        $this->dataTables = $dataTables;
    }
    
    /**
    * Get withdraw data, if status is not null then send data sa per the status 
    *
    * @param string $status
    * @return JsonResponse
    */
    public function getWithdrawList(): JsonResponse
    {
        return $this->dataTables->of($this->model->query())
        ->editColumn('created_at', function($withDraw)
        {
            return date("d-m-yy", strtotime($withDraw->created_at));
        })
        ->editColumn('success_date', function($withDraw)
        {
            if(is_null($withDraw->success_date)) return '-';
            
            return date("d-m-yy", strtotime($withDraw->success_date));
        })
        ->addColumn('user_name', function ($withDraw) {
            return $withDraw->user->name;
        })
        ->addIndexColumn()
        ->make(true);
    }

    /**
    * Get withdraw data, if status is not null then send data sa per the status 
    *
    * @param string $status
    * @return JsonResponse
    */
    public function getWithdrawData(string $status=null): JsonResponse
    {
        $table = $this->model->when($status, function($query, $status) {
            return $query->where('status',  $status);
        });

        return $this->dataTables->of($table)
        ->editColumn('created_at', function($userTransaction)
        {
            return date("d-m-yy", strtotime($userTransaction->created_at));
        })
        ->addColumn('user_name', function ($userTransaction) {
            return $userTransaction->user->name;
        })
        ->addColumn('action', function ($userTransaction) use ($status) {
            if($status == 'pending') {
                return '<div>
                <button id="changeWithDrawStatus" data-id="'.$userTransaction->id.'" class="btn btn-outline-success btn-sm"><i class="fas fa-check"></i></button>
                </div>';
            } else {
                return '';
            }
        })
        ->addIndexColumn()
        ->make(true);
    }
}
