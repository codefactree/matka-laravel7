<?php

namespace App\Repository;

use App\Model\AccountStatement;
use App\Model\User;
use Yajra\DataTables\DataTables;

class AccountStatementRepository extends Repository
{
	/**
	* AccountStatement model object
	* @var AccountStatement
	*/
	protected $model;
	
	public function __construct(AccountStatement $model)
	{
		$this->model = $model;
	}
	
	/**
     * Get Account statement
     *
     * @param User $user
     * @return void
     */
    public function getAccountStatement(User $user)
    {
        return $this->model->where('user_id', $user->id)->orderBy('id', 'DESC')->paginate(10);
    }
	
	/**
     * Get Account statement for Data table in web view
     *
     * @param User $user
     * @return void
     */
    public function getAccountStatementForDatable(User $user)
    {
		return DataTables::of($this->model->where('user_id', $user->id)->orderBy('id', 'DESC'))
		->editColumn('date', function($accountSatement) {
            return date("d-m-Y", strtotime($accountSatement->date));
        })
        ->addIndexColumn()
        ->make(true);
    }
}
