<?php

namespace App\Repository;

use App\Model\Game;

class GameRepository extends Repository
{
	/**
     * ResultDetail model object
     * @var User
     */
	protected $model;
	
	public function __construct(Game $model)
	{
		$this->model = $model;
	}
}
