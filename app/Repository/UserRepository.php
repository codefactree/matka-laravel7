<?php

namespace App\Repository;

use App\Model\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

class UserRepository extends Repository
{
	/**
     * User model object
     * @var User
     */
	protected $model;
	
	public function __construct(User $model)
	{
		$this->model = $model;
	}

	/**
	 * Get All Users from database
	 *
	 * @return JsonResponse
	 */
	public function getAllUsers(): JsonResponse
	{
        return Datatables::of($this->model->query())
        ->addColumn('action', function ($user) {
            $display = $user->id != Auth::user()->id ? 'inline' : 'none';
            return '
            <div>
            <a href="/admin/user/'.$user->id.'" class="btn btn-outline-success btn-sm"><i class="fas fa-eye"></i></a>
            <a href="/admin/user/'.$user->id.'/edit" class="btn btn-outline-primary btn-sm"><i class="fas fa-edit"></i></a>
            <button id="walletModalButton" data-id="'.$user->id.'" class="btn btn-outline-warning btn-sm"><i class="fas fa-wallet"></i></button>
            <a href="/admin/user/'.$user->id.'/account-statement" class="btn btn-outline-success btn-sm"><i class="fas fa-book"></i></a>
            <button id="deleteModalButton" style="display:'.$display.'" data-id="'.$user->id.'" class="btn btn-outline-danger btn-sm"><i class="fas fa-trash"></i></button>
            </div>';
        })
        ->addIndexColumn()
        ->make(true);
	}
}
