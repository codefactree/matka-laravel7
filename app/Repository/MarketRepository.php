<?php

namespace App\Repository;

use App\Model\Market;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Yajra\Datatables\Datatables;

class MarketRepository extends Repository
{
    /**
    * Market model object
    * @var Market
    */
    protected $model;
    
    public function __construct(Market $model)
    {
        $this->model = $model;
    }
    
    /**
    * Get All Markets from database
    *
    * @return JsonResponse
    */
    public function getAllMarkets(): JsonResponse
    {
        return Datatables::of($this->model->query()->orderBy('sort_order'))
        ->editColumn('open_time', function($market) {
            return date("h:i a", strtotime($market->open_time));
        })
        ->editColumn('close_time', function($market) {
            return date("h:i a", strtotime($market->close_time));
        })
        ->addColumn('action', function ($market) {
            return '
            <div>
            <button id="viewModalButton" data-id="'.$market->id.'" class="btn btn-outline-success btn-sm"><i class="fas fa-eye"></i></button>
            <button id="editModalButton" data-id="'.$market->id.'" class="btn btn-outline-primary btn-sm"><i class="fas fa-edit"></i></button>
            <button id="deleteModalButton" data-id="'.$market->id.'" class="btn btn-outline-danger btn-sm"><i class="fas fa-trash"></i></button>
            </div>';
        })
        ->addIndexColumn()
        ->make(true);
    }
    
    /**
    * Get All Markets for resultPage from database
    *
    * @return JsonResponse
    */
    public function getAllMarketForResult(string $date): JsonResponse
    {
        return Datatables::of($this->model->query()->orderBy('sort_order'))
        ->editColumn('open_time', function($market) {
            return date("h:i a", strtotime($market->open_time));
        })
        ->editColumn('close_time', function($market) {
            return date("h:i a", strtotime($market->close_time));
        })
        ->addColumn('open_action', function ($market) use ($date) {
            $result= $market->result($date)->first();
            $isResultGenerated = null;
            if($result) {
                $isResultGenerated = $result->is_open_result_generated ? 'disabled' : null;
            }
            return '
            <div>
            <button id="viewOpenModalButton" data-id="'.$market->id.'" class="btn btn-outline-success btn-sm"><i class="fas fa-eye"></i></button>
            <button id="openEditModalButton" data-id="'.$market->id.'" '.$isResultGenerated.' class="btn btn-outline-primary btn-sm"><i class="fas fa-edit"></i></button>
            <button id="deleteOpenModalButton" data-id="'.$market->id.'" '.$isResultGenerated.' class="btn btn-outline-danger btn-sm"><i class="fas fa-trash"></i></button>
            <a href="/admin/result/generate/open/'.$market->id.'?date='.$date.'" id="openResultGenerateButton" class="btn bg-purple btn-sm"><i class="fas fa-gavel"></i></a>
            </div>';
        })
        ->addColumn('close_action', function ($market) use ($date) {
            $result= $market->result($date)->first();
            $isResultGenerated = null;
            if($result) {
                $isResultGenerated = $result->is_close_result_generated ? 'disabled' : null;
            }
            return '
            <div>
            <button id="viewCloseModalButton" data-id="'.$market->id.'" class="btn btn-outline-success btn-sm"><i class="fas fa-eye"></i></button>
            <button id="editCloseModalButton" data-id="'.$market->id.'" '.$isResultGenerated.' class="btn btn-outline-primary btn-sm"><i class="fas fa-edit"></i></button>
            <button id="deleteCloseModalButton" data-id="'.$market->id.'" '.$isResultGenerated.' class="btn btn-outline-danger btn-sm"><i class="fas fa-trash"></i></button>
            <a href="/admin/result/generate/close/'.$market->id.'?date='.$date.'" id="closeResultGenerateButton" class="btn bg-purple btn-sm"><i class="fas fa-gavel"></i></a>
            </div>';
        })
        ->addColumn('details_action', function ($market) {
            return '
            <div>
            <a id="detailModalButton" href="result/details/'.$market->id.'" class="btn btn-outline-success btn-sm"><i class="fas fa-eye"></i></a>
            </div>';
        })
        ->rawColumns(['open_action','close_action', 'details_action'])
        ->addIndexColumn()
        ->make(true);
    }

    /**
     * Get market list sort by 'sort_order" col
     *
     * @return Collection
     */
    public function getMarketLists(): Collection
    {
        return $this->model->orderBy('sort_order')->get();
    }
}