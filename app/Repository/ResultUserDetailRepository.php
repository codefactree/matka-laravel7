<?php

namespace App\Repository;

use App\Model\ResultUserDetail;

class ResultUserDetailRepository extends Repository
{
	/**
     * ResultDetail model object
     * @var User
     */
	protected $model;
	
	public function __construct(ResultUserDetail $model)
	{
		$this->model = $model;
	}
}
