<?php

namespace App\Repository;

use App\Model\Market;
use App\Model\UserBidDetail;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class UserBidDetailsRepository extends Repository
{
	/**
     * UserBidDetail model object
     * @var UserBidDetail
     */
	protected $model;
	
	public function __construct(UserBidDetail $model)
	{
		$this->model = $model;
    }
    
   /**
    * Get Report Detail i.e. amount on the number as per the game
    *
    * @param Market $market
    * @param string $gameType
    * @param string $date
    * @return Collection
    * @throws Exception
    */
    public function getDetailReport(Market $market, string $gameType, string $date)
    {
        $formattedDate = date("yy-m-d", strtotime($date));
        try {
            return DB::table('user_bids')
            ->leftJoin('user_bid_details', 'user_bids.id', '=', 'user_bid_details.user_bid_id')
            ->leftJoin('markets', 'user_bids.market_id', '=', 'markets.id')
            ->where('market_id', $market->id)
            ->where('game_type', $gameType)
            ->where('date', $formattedDate)
            ->select('game_id', 'bidding_no', DB::raw('sum(amount) as total'))
            ->groupBy('game_id','bidding_no')
            ->get();
           
        } catch (Exception $exception) {
           return $exception;
        }
       
    }

    /**
    * Get Report Detail i.e. amount on the number as per the game
    *
    * @param Market $market
    * @param string $gameType
    * @param string $date
    * @return Collection
    * @throws Exception
    */
    public function getEachGameTotal(Market $market, string $gameType, string $date)
    {
        $formattedDate = date("yy-m-d", strtotime($date));
        try {
            return DB::table('user_bids')
            ->leftJoin('user_bid_details', 'user_bids.id', '=', 'user_bid_details.user_bid_id')
            ->leftJoin('markets', 'user_bids.market_id', '=', 'markets.id')
            ->where('market_id', $market->id)
            ->where('game_type', $gameType)
            ->where('date', $formattedDate)
            ->select('game_id', 'bidding_no', DB::raw('sum(amount) as total'))
            ->groupBy('game_id')
            ->get();
            
        } catch (Exception $exception) {
           return $exception;
        }
       
    }
}
