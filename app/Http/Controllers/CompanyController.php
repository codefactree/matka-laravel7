<?php

namespace App\Http\Controllers;

use App\Model\Company;
use Exception;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            Company::where('id', 1)->update([
                'owner_name' => $request->owner_name,
                'mobile_no' => $request->mobile_no,
            ]);

            return redirect()->route('profile.edit');
        } catch (Exception $exception) {
            return back()->withErrors($exception->getMessage())->withInput();
        }
    }
}
