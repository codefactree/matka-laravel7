<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\PushNotificationService;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PushNotificationController extends Controller
{
    /**
    * PushNotificationService service object.
    *
    * @var PushNotificationService
    */
    protected $pushNotificationService;
    
    public function __construct(PushNotificationService $pushNotificationService)
    {
        $this->pushNotificationService = $pushNotificationService;
    }

    /**
     * Load Push Notifcation Form View
     *
     * @return View
     */
    public function index(): View
    {
        return view('pushNotification.index');
    }

    /**
     * Send Custom Notification to user
     *
     * @param Request $request
     * @return void
     */
    public function send(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required'
        ]);
        
        $this->pushNotificationService->sendCustomNotification($request->all());
        return back()->withSuccess('Notification Sent');
    }
}
