<?php

namespace App\Http\Controllers;

use App\Model\Company;
use App\Model\User;
use Exception;
use GuzzleHttp\Client;
use Illuminate\View\View;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Show the application dashboard.
     *
     * @return View
     */
    public function index(): View
    {
        return view('dashboard.index',[
            'company' => Company::find(1),
            'message' => $this->getAvailableMessageCount(),
            'admin' => User::where('role_id', 1)->count(),
            'user' => User::where('role_id', 2)->count(),
        ]);
    }

    private function getAvailableMessageCount(): string
    {
        try{
          
            $response = $this->client->get('http://whysms.in/app/miscapi/'.config('constant.sms.api_key').'/getBalance/true/');
            $responseBody = json_decode($response->getBody()->getContents());

            return $responseBody[0]->BALANCE;
        } catch (Exception $exception) {
            return 'NA';
        }
    }
}
