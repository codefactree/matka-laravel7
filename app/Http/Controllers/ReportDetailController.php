<?php

namespace App\Http\Controllers;

use App\Model\Market;
use App\Services\ReportDetailService;
use App\Services\ResultService;
use Illuminate\Http\Request;
use Illuminate\View\View;
use Exception;

class ReportDetailController extends Controller
{
    /**
    * ReportDetailService service object.
    *
    * @var 
    */
    protected $reportService;
    
    /**
    * ResultService service object.
    *
    * @var 
    */
    protected $resultService;
    
    public function __construct(ReportDetailService $reportService, ResultService $resultService)
    {
        $this->reportService = $reportService;
        $this->resultService = $resultService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Market $market): View
    {
        $results = $this->reportService->getDetailReport($market,$request->get('game-type'), $request->get('date'));
        if($results instanceof Exception) {
            return back()->withErrors($results->getMessage())->withInput();
        }
       
        $date = convertDMYtoYMD($request->get('date'));
        $totalAmount = $this->resultService->getTotalMarketAmountForReport($market,$request->get('game-type'), $date);
        if($totalAmount instanceof Exception) {
            return back()->withErrors($totalAmount->getMessage())->withInput();
        }
        
        $eachGameTotal = $this->reportService->getEachGameTotal($market,$request->get('game-type'), $date);
        if($eachGameTotal instanceof Exception) {
            return back()->withErrors($eachGameTotal->getMessage())->withInput();
        }
        
        return view('reportDetail.index',[
            'results' => $results,
            'market' => $market,
            'totalAmount' => $totalAmount,
            'eachGameTotal' => reset($eachGameTotal),
        ]);
    }
}
