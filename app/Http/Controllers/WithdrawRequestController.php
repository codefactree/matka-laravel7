<?php

namespace App\Http\Controllers;

use App\Services\WithdrawRequestService;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

class WithdrawRequestController extends Controller
{
    /**
    * WithdrawRequestService service object.
    *
    * @var WithdrawRequestService
    */
    protected $withdrawRequestService;
    
    public function __construct(WithdrawRequestService $withdrawRequestService) 
    {
        $this->withdrawRequestService = $withdrawRequestService;
    }

    public function index(): View
    {
        return view('withdraw.index');
    }

    /**
     * Fetch all the Withdraw list
     *
     * @return JsonResponse
     */
    public function getWithdrawList(): JsonResponse
    {
        return $this->withdrawRequestService->getWithdrawList();
    }

    /**
     * Fetch all the Pending Withdraw request
     *
     * @return void
     */
    public function getPendingWithdrawRequest()
    {
        return $this->withdrawRequestService->getWithdrawData('pending');
    }

    /**
     * Change User withdraw request status to completed
     *
     * @param integer $transactionId
     * @return bool
     */
    public function changeWithdrawStatus(int $transactionId): bool
    {
        return $this->withdrawRequestService->changeWithdrawStatus($transactionId);
    }
}
