<?php

namespace App\Http\Controllers;

use App\Http\Requests\Result\CloseResultFormRequest;
use App\Http\Requests\Result\OpenResultFormRequest;
use App\Model\Game;
use App\Model\Market;
use App\Model\Result;
use App\Model\ResultDetail;
use App\Services\AccountStatementService;
use App\Services\GameService;
use App\Services\MarketService;
use App\Services\PushNotificationService;
use App\Services\ResultDetailService;
use App\Services\ResultDetailUserService;
use App\Services\ResultService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class ResultController extends Controller
{
    /**
    * MarketService service object.
    *
    * @var MarketService
    */
    protected $marketService;
    
    /**
    * ResultService service object.
    *
    * @var ResultService
    */
    protected $resultService;
    
    public function __construct(
        MarketService $marketService, 
        ResultService $resultService,
        ResultDetailService $resultDetailService,
        ResultDetailUserService $resultDetailUserService,
        UserService $userService,
        GameService $gameService,
        AccountStatementService $accountStatementService,
        PushNotificationService $pushNotificationService
    ) {
        $this->marketService = $marketService;
        $this->resultService = $resultService;
        $this->resultDetailService = $resultDetailService;
        $this->resultDetailUserService = $resultDetailUserService;
        $this->userService = $userService;
        $this->gameService = $gameService;
        $this->accountStatementService = $accountStatementService;
        $this->pushNotificationService = $pushNotificationService;
    }
    
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(): View
    {
        return view('result.index');
    }
    
    /**
    * Get All Results from database
    *
    * @return JsonResponse
    */
    public function getAllResult(string $date): JsonResponse
    {
        return $this->marketService->getAllMarketForResult($date);
    }
    
    /**
    * Get Todays Result for the market
    *
    * @return array
    */
    public function resultForMarket(Market $market): array
    {
        return [
            'market' => $market,
            'result' => $market->result(getTodayDateYMD())->first()
        ];
    }
    
    /**
    * Store result number for the market as per the type
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function storeOpenResult(OpenResultFormRequest $request): Result
    {
        $result = $this->resultService->storeResult($request->all(), 'open');
        $this->pushNotificationService->sendResultNotification($result, 'open');
        return $result;
    }

    /**
    * Store result number for the market as per the type
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function storeCloseResult(CloseResultFormRequest $request)
    {
        $findResult = $this->resultService->findResultAsPerCloseMarketData($request->all(), 'close');
        if(is_null($findResult) || is_null($findResult->first3)) {
            return response()->json(generic_error_response('Result for open is yet to be generated'));
            throw new Exception('Result for open is yet to be generated');
        }
      
        $result = $this->resultService->storeResult($request->all(), 'close');
        $this->pushNotificationService->sendResultNotification($result, 'close');
        return $result;
    }
    
    /**
    * Delete Open result
    * Make the open_time & mid1 field of the selected market Blank
    *
    * @param Market $market
    * @return boolean
    */
    public function deleteOpenResult(Market $market): bool
    {
        return $this->resultService->deleteOpenResult($market, 'open');
    }
    
    /**
    * Delete Close result
    * Make the close_time & mid2 field of the selected market Blank
    *
    * @param Market $market
    * @return boolean
    */
    public function deleteCloseResult(Market $market): bool
    {
        return $this->resultService->deleteOpenResult($market, 'close');
    }

    /**
    * Get Result Details View for the market
    *
    * @return array
    */
    public function resultDetails(Market $market): View
    {
        return view('result.resultDetails', [
            'market' => $market
        ]);
    }

    /**
    * Get Result Details for the market
    *
    * @param Market $market
    * @return JsonResponse
    */
    public function resultDetailsForMarket(Market $market): JsonResponse
    {
        return $this->resultService->getResultDetailsForMarket($market);
    }

    /**
    * Generate Result for the Open market showing total winning amount as per the bidding
    *
    * @param Market $market
    * @return View
    */
    public function generateOverallOpenResult(Request $request, Market $market)
    {
        $date = $request->input('date');
        $totalAmount = $this->resultService->getTotalAmountForMarket($market, 'open', $date);
        $marketResult = $market->result($date)->first();
        if (is_null($marketResult)) {
            return back()->withErrors('Result not generated yet');
        }

        if (is_null($marketResult->first3)) {
            return back()->withErrors('Result for open is not generated yet');
        }
        
        try {
            $singleResults = $this->resultService->generateOverallResult($market, 1, $marketResult->mid1, $date);
            $singlePattiResults = $this->resultService->generateOverallResult($market, 3, $marketResult->first3, $date);
            $doublePattiResults = $this->resultService->generateOverallResult($market, 4, $marketResult->first3, $date);
            $tripplePattiResults = $this->resultService->generateOverallResult($market, 5, $marketResult->first3, $date);
            
            $results = [
                'singleResults' => $singleResults, 
                'singlePattiResults' => $singlePattiResults, 
                'doublePattiResults' => $doublePattiResults, 
                'tripplePattiResults' => $tripplePattiResults
            ];
        } catch(Exception $exception) {
            return back()->withErrors($exception->getMessage());
        }

        return view('resultDetail.open.index', [
            'market' => $market,
            'totalAmount' => $totalAmount,
            'marketResult' => $marketResult,
            'results' => $results,
            'games' => $this->gameService->getAllGames()
        ]);
    }

    /**
    * Generate Overall Result for the Close market showing total winning amount as per the bidding
    *
    * @param Market $market
    * @return View
    */
    public function generateOverallCloseResult(Request $request, Market $market)
    {
        $date = $request->input('date');
        $totalAmount = $this->resultService->getTotalAmountForMarket($market, 'close', $date);
        $marketResult = $market->result($date)->first();
       
        if (is_null($marketResult)) {
            return back()->withErrors('Result not generated yet');
        }

        if (is_null($marketResult->first3)) {
            return back()->withErrors('Result for open is not generated yet');
        }

        if (is_null($marketResult->last3)) {
            return back()->withErrors('Result for close is not generated yet');
        }

        try {
            $jodiResults = $this->resultService->generateOverallResult($market, 2, $marketResult->mid1.$marketResult->mid2, $date);
            $halfSangamResults = $this->resultService->generateOverallHalfSangamResult($market, 6, $marketResult, $date);
            $fullSangamResults = $this->resultService->generateOverallResult($market, 7, $marketResult->first3.'--'.$marketResult->last3, $date);
            $singleResults = $this->resultService->generateOverallResult($market, 8, $marketResult->mid2, $date);
            $singlePattiResults = $this->resultService->generateOverallResult($market, 9, $marketResult->last3, $date);
            $doublePattiResults = $this->resultService->generateOverallResult($market, 10, $marketResult->last3, $date);
            $tripplePattiResults = $this->resultService->generateOverallResult($market, 11, $marketResult->last3, $date);
           
            $results = [
                'jodiResults' => $jodiResults, 
                'halfSangamResults' => $halfSangamResults, 
                'fullSangamResults' => $fullSangamResults, 
                'singleResults' => $singleResults, 
                'singlePattiResults' => $singlePattiResults, 
                'doublePattiResults' => $doublePattiResults, 
                'tripplePattiResults' => $tripplePattiResults
            ];
        } catch(Exception $exception) {
            return back()->withErrors($exception->getMessage());
        }

        return view('resultDetail.close.index', [
            'market' => $market,
            'totalAmount' => $totalAmount,
            'marketResult' => $marketResult,
            'results' => $results,
            'games' => $this->gameService->getAllGames()
        ]);
    }

    /**
     * Result Detail for Market with user wise detail
     *
     * @param ResultDetail $resultDetail
     */
    public function resultUserDetail(Request $request, Market $market, Game $game)
    {
        $date = $request->input('date');
        $marketResult = $market->result($date)->first();
        $biddingNo = $this->getBiddingNo($game->id, $marketResult);
        $resultUserDetails = $this->resultService->generateResultUserwise($market, $game->id, $biddingNo, $date);
        $resultTotalDetail = $this->resultService->generateTotalAmountResultUserwise($market, $game->id, $biddingNo, $date);

        return view('resultUserDetail.index', [
            'market' => $market,
            'selectedGame' => $game,
            'resultTotalDetail' => $resultTotalDetail,
            'resultUserDetails' => $resultUserDetails,
            'games' => $this->gameService->getAllGames()
        ]);
    }

    /**
     * Get Bidding number as per the game
     *
     * @param integer $gameId
     * @param Result $result
     * @return string|array
     */
    private function getBiddingNo(int $gameId, Result $result)
    {
        if($gameId == 1) { //single
            return $result->mid1;
        } elseif($gameId == 2) { //jodi
            return $result->mid1. $result->mid2;
        } elseif($gameId == 3 || $gameId == 4 || $gameId == 5) { // single patti, double patti, tripple patti
            return $result->first3;
        } elseif($gameId == 6) { //half sangam
            return [$result->mid1.'--'.$result->last3, $result->first3.'--'.$result->mid2];
        } elseif($gameId == 7) { //full sangam
            return $result->first3.'--'.$result->last3;
        } elseif($gameId == 8) { //single
            return $result->mid2;
        } elseif($gameId == 9 || $gameId == 10 || $gameId == 11) { // single patti, double patti, tripple patti
            return $result->last3;
        }
    }

    public function frozeResultAndTransferMoney(Request $request)
    {
        $market = Market::find($request->marketId);
        if ($request->type == 'open') {
            return $this->generateSummarizeOpenResult($request->all(), $market);
        } else {
            return $this->generateSummarizeCloseResult($request->all(), $market);
        }
    }

    /**
     * Generate Summartize result for Saving in db
     *
     * @param array $request
     * @param Market $market
     */
    private function generateSummarizeOpenResult(array $request, Market $market)
    {
        $result = $market->result($request['date'])->first();
        $date = $request['date'];
        try {
            DB::beginTransaction();
            if ($result->is_open_result_generated) {
                return back()->withErrors('Result already Generated ');
            }
                $singleResults = $this->resultService->generateResultToFreeze($market, 1, $result->mid1, $date);
                $singlePattiResults = $this->resultService->generateResultToFreeze($market, 3, $result->first3, $date);
                $doublePattiResults = $this->resultService->generateResultToFreeze($market, 4, $result->first3, $date);
                $tripplePattiResults = $this->resultService->generateResultToFreeze($market, 5, $result->first3, $date);

                $saveSingleResults = $this->saveResultDetail($result, $singleResults);
                if ($saveSingleResults instanceof Exception) {
                    DB::rollBack();
                    return back()->withErrors($saveSingleResults->getMessage());
                }
                
                $saveSinglePattiResults = $this->saveResultDetail($result, $singlePattiResults);
                if ($saveSinglePattiResults instanceof Exception) {
                    DB::rollBack();
                    return back()->withErrors($saveSinglePattiResults->getMessage());
                }

                $saveDoublePattiResults = $this->saveResultDetail($result, $doublePattiResults);
                if ($saveDoublePattiResults instanceof Exception) {
                    DB::rollBack();
                    return back()->withErrors($saveDoublePattiResults->getMessage());
                }

                $saveTripplePattiResults = $this->saveResultDetail($result, $tripplePattiResults);
                if ($saveTripplePattiResults instanceof Exception) {
                    DB::rollBack();
                    return back()->withErrors($saveTripplePattiResults->getMessage());
                }

                $this->resultService->resultIsGenerated($result, 'open');
           
            DB::commit();
            return back()->withSuccess('Successfully Transfered the Money');
        } catch(Exception $exception) {
            DB::rollBack();
            return back()->withErrors($exception->getMessage());
        }
    }

    /**
     * Generate Summartize result for Saving in db
     *
     * @param array $request
     * @param Market $market
     */
    private function generateSummarizeCloseResult(array $request, Market $market)
    {
        $result = $market->result($request['date'])->first();
        $date = $request['date'];
        try {
            DB::beginTransaction();
            if ($result->is_close_result_generated) {
                return back()->withErrors('Close result already Generated');
            }

            $jodiResults = $this->resultService->generateResultToFreeze($market, 2, $result->mid1.$result->mid2, $date);
            $halfSangamResults = $this->resultService->generateResultToFreeze($market, 6, $result, $date);
            $fullSangamResults = $this->resultService->generateResultToFreeze($market, 7, $result->first3.'--'.$result->last3, $date);
            $singleResults = $this->resultService->generateResultToFreeze($market, 8, $result->mid2, $date);
            $singlePattiResults = $this->resultService->generateResultToFreeze($market, 9, $result->last3, $date);
            $doublePattiResults = $this->resultService->generateResultToFreeze($market, 10, $result->last3, $date);
            $tripplePattiResults = $this->resultService->generateResultToFreeze($market, 11, $result->last3, $date);

            $saveSingleResults = $this->saveResultDetail($result, $singleResults);
            if ($saveSingleResults instanceof Exception) {
                DB::rollBack();
                return back()->withErrors($saveSingleResults->getMessage());
            }

            $saveJodiResults = $this->saveResultDetail($result, $jodiResults);
            if ($saveJodiResults instanceof Exception) {
                DB::rollBack();
                return back()->withErrors($saveJodiResults->getMessage());
            }

            $saveHalfSangamResults = $this->saveResultDetail($result, $halfSangamResults);
            if ($saveHalfSangamResults instanceof Exception) {
                DB::rollBack();
                return back()->withErrors($saveHalfSangamResults->getMessage());
            }

            $saveFullSangamResults = $this->saveResultDetail($result, $fullSangamResults);
            if ($saveFullSangamResults instanceof Exception) {
                DB::rollBack();
                return back()->withErrors($saveFullSangamResults->getMessage());
            }
            
            $saveSinglePattiResults = $this->saveResultDetail($result, $singlePattiResults);
            if ($saveSinglePattiResults instanceof Exception) {
                DB::rollBack();
                return back()->withErrors($saveSinglePattiResults->getMessage());
            }

            $saveDoublePattiResults = $this->saveResultDetail($result, $doublePattiResults);
            if ($saveDoublePattiResults instanceof Exception) {
                DB::rollBack();
                return back()->withErrors($saveDoublePattiResults->getMessage());
            }
            
            $saveTripplePattiResults = $this->saveResultDetail($result, $tripplePattiResults);
            if ($saveTripplePattiResults instanceof Exception) {
                DB::rollBack();
                return back()->withErrors($saveTripplePattiResults->getMessage());
            }

            $this->resultService->resultIsGenerated($result, 'close');
            DB::commit();
            
            return back()->withSuccess('Successfully Transfered the Money');
        } catch(Exception $exception) {
            DB::rollBack();
            return back()->withErrors($exception->getMessage());
        }
    }


     /**
     * Save Result Details in database
     *
     * @param Collection $resultCollection
     * @return void
     */
    private function saveResultDetail(Result $result, Collection $resultCollection)
    {
        if ($resultCollection->isNotEmpty()) {
            //save result data
            $resultDetail = $this->resultDetailService->saveResultDetail($result, $resultCollection[0], $resultCollection->sum('amount'));
           
            // save result with user bid and amount data 
            foreach ($resultCollection as $data) {
               $res = $this->resultDetailUserService->saveResultDetailUser($data, $resultDetail);
               if($res instanceof Exception) {
                   return $res;
               }
            }

            //transfer the money to the user
            $transferMoney = $this->transferMoney($resultDetail);
            if($transferMoney instanceof Exception) {
                return $transferMoney;
            }
        }
        return true;
    }

    /**
     * Transfer money to winning user
     *
     * @param ResultDetail $resultDetail
     */
    private function transferMoney(ResultDetail $resultDetail)
    {
        try {
            foreach ($resultDetail->resultUserDetail as $resultUserDetail) {
                $winningAmount = floor($resultUserDetail['amount'] * config('constant.rate')[$resultDetail->game_id]['multiplicityRate']);
                $this->userService->addMoneyToWallet(['amount' => $winningAmount] ,$resultUserDetail->user);
    
                $this->accountStatementService->saveUserWinningDetail($resultUserDetail, $winningAmount);
            }
            $this->resultDetailService->moneyIsTransfered($resultDetail);

            return true;
        } catch (Exception $exception) {
            return $exception;
        }
    }
}
