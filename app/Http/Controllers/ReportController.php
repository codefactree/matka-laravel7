<?php

namespace App\Http\Controllers;

use App\Services\ReportService;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

class ReportController extends Controller
{
    /**
    * ReportService service object.
    *
    * @var 
    */
    protected $reportService;
    
    public function __construct(ReportService $reportService)
    {
        $this->reportService = $reportService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(): View
    {
        return view('report.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function getReport(string $date): JsonResponse
    {
        return $this->reportService->getReport($date);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getTotalBidAmount(string $date)
    {
        return $this->reportService->getTotalBidAmount($date);
    }
}
