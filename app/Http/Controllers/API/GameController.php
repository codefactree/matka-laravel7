<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Model\Result;
use App\Model\User;
use App\Services\AccountStatementService;
use App\Services\GameService;
use App\Services\UserBidDetailsService;
use App\Services\UserBidService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    /**
    * UserBidService service object.
    *
    * @var UserBidService
    */
    protected $userBidService;

    /**
    * UserBidDetailsService service object.
    *
    * @var UserBidDetailsService
    */
    protected $userBidDetailsService;

    /**
    * GameService service object.
    *
    * @var GameService
    */
    protected $gameService;
    
    /**
    * UserService service object.
    *
    * @var UserService
    */
    protected $userService;

    /**
    * AccountStatementService service object.
    *
    * @var AccountStatementService
    */
    protected $accountStatementService;
    
    public function __construct(
        UserBidService $userBidService,
        UserBidDetailsService $userBidDetailsService,
        GameService $gameService,
        UserService $userService,
        AccountStatementService $accountStatementService
    ) {
        $this->userBidService = $userBidService;
        $this->userBidDetailsService = $userBidDetailsService;
        $this->gameService = $gameService;
        $this->userService = $userService;
        $this->accountStatementService = $accountStatementService;
    }
    
    /**
     * Get all games list
     */
    public function getAllGames(): JsonResponse
    {
        return response()->json(successResponse(['games' => $this->gameService->getAllGames()]));
    }

    /**
     * Submit Bid for the game
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function submitBid(Request $request): JsonResponse
    {
        $user = Auth::user();
        $this->validateSubmitDetails($request, $user);
        $result = (Result::where('market_id', $request->market_id)->where('date', getTodayDateYMD())->first()); // can bid for today only
        if ($result) {
            if($request->game_id <= 7) {
                if( $result->is_open_result_generated) {
                    return response()->json(generic_error_response('Cant Bid Now as, Open Result already generated'));
                }
            } 
            if($request->game_id >= 8) {
                if( $result->is_close_result_generated) {
                    return response()->json(generic_error_response('Cant Bid Now as, Close Result already generated'));
                }
            } 
        }
        
        DB::beginTransaction();
        $userBidServiceResult = $this->userBidService->saveUserBid($request->all(), $user);
        if ($userBidServiceResult instanceof Exception) { 
            //if there's an exception while storing userBidServiceResult data throw error
            DB::rollBack();
            return response()->json(errorResponse($userBidServiceResult));
        }

        foreach ($request->all()['bids'] as $bid) {
            $userBidDetailsServiceResult = $this->userBidDetailsService->saveUserBidDetails($bid, $userBidServiceResult);
            
            if ($userBidDetailsServiceResult instanceof Exception) { 
                DB::rollBack();
                
                return response()->json(errorResponse($userBidDetailsServiceResult));
            }
            $this->userService->debitWaletAmount(['amount' => $bid['amount']], $user);
            $this->accountStatementService->logUserBidDetails($bid, $userBidServiceResult);
        }
        
        DB::commit();
        
        return response()->json(successResponse(['message' => 'Bid has been submitted sucessfully']));
    }

    /**
     * Validate Submit Details
     *
     * @param Request $request
     * @param User $user
     * @return void
     */
    private function validateSubmitDetails(Request $request, User $user)
    {
        $request->validate(
            [
                'game_id' => 'required|exists:games,id',
                'bids.*.bidding_no' => 'required',
                'bids.*.amount' => 'required|lte:'.$user->wallet_amount,
                'total_amount' => 'required|lte:'.$user->wallet_amount
            ]
        );
    }

    /**
     * Get all result value
     *
     * @return JsonResponse
     */
    public function allResultValues(): JsonResponse
    {
        return response()->json(successResponse([
            'single_patti' => config('constant.single_patti'),
            'double_patti' => config('constant.double_patti'),
            'tripple_patti' => config('constant.tripple_patti'),
        ]));
    }
}
