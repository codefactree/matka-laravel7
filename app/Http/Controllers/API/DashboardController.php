<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\CompanyService;
use App\Services\MarketService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class DashboardController extends Controller
{
    /**
    * MarketService service object.
    *
    * @var MarketService
    */
    protected $marketService;
    
    /**
    * CompanyService service object.
    *
    * @var CompanyService
    */
    protected $companyService;
    
    public function __construct(
        MarketService $marketService,
        CompanyService $companyService
    ) {
        $this->marketService = $marketService;
        $this->companyService = $companyService;
    }
    
    /**
     * Get details of market, logged user & Company details 
     * to show on app
     *
     * @return JsonResponse
     */
    public function getDashboardDetails(): JsonResponse
    {
        $market = $this->marketService->getAllMarketDetailsForApi();
        if($market instanceof Exception) { //if there's an exception while fetching Market data throw error
            return response()->json(errorResponse($market));
        }
        
        $company = $this->companyService->getCompanyDetails();
        if($company instanceof Exception) { //if there's an exception while fetching Company data throw error
            return response()->json(errorResponse($company));
        }

        return response()->json(successResponse([
            'user' => Auth::user()->load('userDetail'),
            'market' => $market,
            'company' => $company
        ]));
    }
}
