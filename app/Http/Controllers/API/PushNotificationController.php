<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Model\Result;
use App\Services\PushNotificationService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PushNotificationController extends Controller
{
    /**
    * UserService service object.
    *
    * @var UserService
    */
    protected $userService;

    /**
    * PushNotificationService service object.
    *
    * @var PushNotificationService
    */
    protected $pushNotificationService;
    
    public function __construct(UserService $userService, PushNotificationService $pushNotificationService)
    {
        $this->userService = $userService;
        $this->pushNotificationService = $pushNotificationService;
    }

    public function saveToken(Request $request): JsonResponse
    {
        $request->validate([
            'fcm_token' => 'required'
        ]);

        $user = Auth::user();
        DB::beginTransaction();
        $result = $this->userService->saveToken($request->all(), $user);
        if ($result instanceof Exception) { 
            //if there's an exception while updating user token data throw error
            DB::rollBack();
            return response()->json(errorResponse($result));
        }
        DB::commit();
        
        return response()->json(successResponse(['message' => 'Token Saved successfully']));
    }

    public function sendNotification()
    {
        $result = Result::find(1);
        $this->pushNotificationService->sendResultNotification($result, 'open');
    }
}
