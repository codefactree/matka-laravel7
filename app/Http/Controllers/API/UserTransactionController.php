<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use App\Services\UserTransactionService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserTransactionController extends Controller
{
    /**
    * UserTransactionService service object.
    *
    * @var UserTransactionService
    */
    protected $userTransactionService;
    
    public function __construct(
        UserTransactionService $userTransactionService,
        UserService $userService
    ) {
        $this->userTransactionService = $userTransactionService;
        $this->userService = $userService;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeWithdrawRequest(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'amount' => 'required|lte:'.$user->wallet_amount
        ]);
        DB::beginTransaction();
        $userTransactionResult = $this->userTransactionService->storeTransactionRequest($request->all(), 'debit');
        if ($userTransactionResult instanceof Exception) { 
            //if there's an exception while fetching Company data throw error
            DB::rollBack();
            return response()->json(errorResponse($userTransactionResult));
        }

        $userWalletTransaction = $this->userService->debitWaletAmount($request->all(), $user);
        if ($userWalletTransaction instanceof Exception) { 
            //if there's an exception while fetching Company data throw error
            DB::rollBack();
            return response()->json(errorResponse($userWalletTransaction));
        }
        DB::commit();

        return response()->json(successResponse(['message' => 'Withdraw request submitted sucessfully']));
    }
}
