<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Services\AccountStatementService;
use App\Services\UserService;
use App\Services\WithdrawRequestService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class WithdrawRequestController extends Controller
{
    /**
    * WithdrawRequestService service object.
    *
    * @var WithdrawRequestService
    */
    protected $withdrawRequestService;
    
    /**
    * UserService service object.
    *
    * @var UserService
    */
    protected $userService;
    
    /**
    * AccountStatementService service object.
    *
    * @var AccountStatementService
    */
    protected $accountStatementService;
    
    public function __construct(
        WithdrawRequestService $withdrawRequestService,
        UserService $userService,
        AccountStatementService $accountStatementService
    ) {
        $this->withdrawRequestService = $withdrawRequestService;
        $this->userService = $userService;
        $this->accountStatementService = $accountStatementService;
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function storeWithdrawRequest(Request $request)
    {
        $user = Auth::user();
        $request->validate([
            'amount' => 'required|lte:'.$user->wallet_amount
            ]);
        DB::beginTransaction();
        $withdrawRequestResult = $this->withdrawRequestService->storeWithdrawRequest($request->all());
        if ($withdrawRequestResult instanceof Exception) { 
            //if there's an exception while fetching Company data throw error
            DB::rollBack();
            return response()->json(errorResponse($withdrawRequestResult));
        }
        
        $userWalletTransaction = $this->userService->debitWaletAmount($request->all(), $user);
        $userWalletTransaction = $this->accountStatementService->logWithdrawRequest($withdrawRequestResult, $user);
        if ($userWalletTransaction instanceof Exception) { 
            //if there's an exception while fetching Company data throw error
            DB::rollBack();
            return response()->json(errorResponse($userWalletTransaction));
        }
        DB::commit();
        
        return response()->json(successResponse(['message' => 'Withdraw request submitted sucessfully']));
    }
}
