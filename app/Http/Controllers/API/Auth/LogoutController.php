<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Services\UserService;
use Exception;
use Illuminate\Support\Facades\Auth;

class LogoutController extends Controller
{
    /**
    * UserService service object.
    *
    * @var UserService
    */
    protected $userService;
    
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function logout()
    {
        try {
            Auth::user()->token()->revoke();
            $result = $this->userService->clearFCMToken(Auth::user());
            if ($result instanceof Exception) { 
                return response()->json(errorResponse($result));
            }

            return response()->json(
                [
                    'status' => 200,
                    'message' => 'Log out successfully'
                ]
        );
        } catch(Exception $exception) {
            return response()->json(errorResponse($exception));
        }
    }
}
