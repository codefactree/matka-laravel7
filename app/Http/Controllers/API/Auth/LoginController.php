<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
    * Get the login username to be used by the controller.
    *
    * @return string
    */
    public function username(): string
    {
        $login = request()->input('userName');
        if(is_numeric($login)){
            $field = 'mobile_no';
        } elseif (filter_var($login, FILTER_VALIDATE_EMAIL)) {
            $field = 'email';
        } else {
            $field = 'user_name';
        }
        request()->merge([$field => $login]);
        
        return $field;
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function attemptLogin(Request $request): bool
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->filled('remember')
        );
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request): JsonResponse
    {
        $accessToken= Auth::user()->createToken('authToken')->accessToken;
        return response()->json(successResponse([
            'accessToken' => $accessToken
        ]));
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        return response()->json([
            'status' => 500,
            'error' => [
                $this->username() => [trans('auth.failed')]
            ]
        ]);
    }
}
