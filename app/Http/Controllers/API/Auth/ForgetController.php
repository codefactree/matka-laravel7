<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Services\AuthService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ForgetController extends Controller
{
    /**
    * UserService service object.
    *
    * @var UserService
    */
    protected $userService;

    /**
    * AuthService service object.
    *
    * @var AuthService
    */
    protected $authService;
    
    public function __construct(
        UserService $userService,
        AuthService $authService
    ) {
        $this->userService = $userService;
        $this->authService = $authService;
    }
    
    /**
     *  Send OTP to the user trying to reset password
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sendOtp(Request $request): JsonResponse
    {
        $this->validateMobileNo($request);
        $result = $this->authService->sendOtp($request->all(), 'forgetPassword');

        if($result == false) {
            return response()->json(generic_error_response('Error while sending OTP'));
        }

        return response()->json([ 
            'status' => 200,
            'message' => 'OTP sent successfully '
        ]);
    }

    /**
     * Validate OTP of the user trying to reset password
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function validateOtp(Request $request): JsonResponse
    {
        $this->validateMobileNo($request);
        $result = $this->authService->validateOtp($request, 'forgetPassword');

        if($result instanceof Exception) {
            return response()->json(errorResponse($result));
        }

        return response()->json([
            'status' => 200,
            'message' => 'OTP verified successfully'
        ]);
    }

    /**
     * Reset user password in db
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function reset(Request $request): JsonResponse
    {
        if (!Cache::get($request->mobile_no . '_forgetPassword_is_verified')) {
            // send error if user is not verified
            return response()->json(generic_error_response('User is not verified'));
        }

        $request->validate([
            'password' => [
                'required',
                'confirmed'
            ],
        ]);
        
        DB::beginTransaction();
        $user = $this->userService->findUser($request->all());
        if ($user instanceof Exception) { 
            //if there's an exception while saving user data throw error
            return response()->json(errorResponse($user));
        }
        $userResetResult = $this->userService->resetPassword($request->all(), $user);
        if ($userResetResult instanceof Exception) { 
            //if there's an exception while saving user data throw error
            DB::rollBack();
            return response()->json(errorResponse($userResetResult));
        }
        
        DB::commit();
        Cache::forget($request->mobile_no . '_forgetPassword_is_verified'); //remove is_verified cache after registration

        return response()->json([
                'status' => 200,
                'message' => 'User Password Reset successfully'
            ]);
    }
    
    /**
     * Validate Mobile Number
     *
     * @param Request $request
     * @return void
     */
    private function validateMobileNo(Request $request)
    {
        $request->validate(
            [
            'mobile_no' => [
                'required',
                'integer',
                'exists:users,mobile_no'
            ]
        ]);
    }
}
