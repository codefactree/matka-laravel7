<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use App\Services\AuthService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class RegisterController extends Controller
{
    /**
    * UserService service object.
    *
    * @var UserService
    */
    protected $userService;

    /**
    * AuthService service object.
    *
    * @var AuthService
    */
    protected $authService;
    
    public function __construct(
        UserService $userService,
        AuthService $authService
    ) {
        $this->userService = $userService;
        $this->authService = $authService;
    }
    
    /**
     * Send OTP to the user trying to regiser
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function sendOtp(Request $request): JsonResponse
    {
        $this->validateMobileNo($request);
        $result = $this->authService->sendOtp($request->all(), 'register');

        if($result == false) {
            return response()->json(generic_error_response('Error while sending OTP'));
        }

        return response()->json([ 
            'status' => 200,
            'message' => 'OTP sent successfully '
        ]);
    }

    /**
     * Validate OTP of the user trying to register
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function validateOtp(Request $request): JsonResponse
    {
        $this->validateMobileNo($request);
        $result = $this->authService->validateOtp($request, 'register');

        if($result instanceof Exception) {
            return response()->json(errorResponse($result));
        }

        return response()->json([ 
            'status' => 200,
            'message' => 'OTP verified successfully'
        ]);
    }

    /**
     * Register user in db
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        if (!Cache::get($request->mobile_no . '_register_is_verified')) {
            // send error if user is not verified
            return response()->json(generic_error_response('User is not verified'));
        }

        $request->validate(
            [
                'user_name' => [
                    'required', 
                    'unique:users,user_name,NULL,id,deleted_at,NULL'
                ],
                'password' => [
                    'required',
                    'confirmed'
                ],
                'mobile_no' => [
                    'required',
                    'integer',
                    'unique:users,mobile_no,NULL,id,deleted_at,NULL'
                ]
        ]);
        
        DB::beginTransaction();
        $user = $this->userService->saveUser($request->all());
        if ($user instanceof Exception) { 
            //if there's an exception while saving user data throw error
            DB::rollBack();
            return response()->json(errorResponse($user));
        }

        Cache::forget($request->mobile_no . '_register_is_verified'); //remove is_verified cache after registration
        $accessToken = $this->authService->loginUser($user);
        DB::commit();
        return response()->json(successResponse([
            'accessToken' => $accessToken
        ]));
    }

    /**
     * Validate Mobile Number
     *
     * @param Request $request
     * @return void
     */
    private function validateMobileNo(Request $request)
    {
        $request->validate(
            [
            'mobile_no' => [
                'required',
                'integer',
                'unique:users,mobile_no,NULL,id,deleted_at,NULL'
            ]
        ]);
    }
}
