<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UpdateUserFormRequest;
use App\Model\Company;
use App\Services\AccountStatementService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    /**
    * UserService service object.
    *
    * @var UserService
    */
    protected $userService;
    
    /**
    * AccountStatementService service object.
    *
    * @var AccountStatementService
    */
    protected $accountStatementService;
    
    public function __construct(UserService $userService, AccountStatementService $accountStatementService)
    {
        $this->userService = $userService;
        $this->accountStatementService = $accountStatementService;
    }
    
    /**
    * Show User & User Detail for Profile
    *
    * @return JsonResponse
    */
    public function getUserDetails(): JsonResponse
    {
        try {
            $user = Auth::user()->load('userDetail');
            return response()->json(successResponse([
                'user' => $user,
                ])
            );
        } catch (Exception $exception) {
            return response()->json(errorResponse($exception));
        }
    }

    /**
     * Update User in Database
     *
     * @param UpdateUserFormRequest $request
     * @return JsonResponse
     */
    public function update(UpdateUserFormRequest $request): JsonResponse
    {
        $user = Auth::user();
        DB::beginTransaction();
        $result = $this->userService->updateUser($request->all(), $user);
        if ($result instanceof Exception) { 
            //if there's an exception while updating userprofile data throw error
            DB::rollBack();
            return response()->json(errorResponse($result));
        }
        DB::commit();
        
        return response()->json(successResponse(['message' => 'Profile Updated sucessfully']));
    }

    /**
     * Get Account Statement
     *
     * @return JsonResponse
     */
    public function getAccountStatement(): JsonResponse
    {
        try {
            $user = $this->accountStatementService->getAccountStatement(Auth::user());
            return response()->json(successResponse([
                'user' => $user,
                ])
            );
        } catch (Exception $exception) {
            return response()->json(errorResponse($exception));
        }
    }

    /**
     * get Company Detail for login page 
     *
     * @return void
     */
    public function getCompany()
    {
        return response()->json(successResponse(['company' => Company::find(1)]));
    }
}
