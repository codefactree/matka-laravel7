<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\UpdateUserFormRequest;
use App\Model\Company;
use App\Model\Role;
use App\Model\User;
use App\Services\UserService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\View\View;

class ProfileController extends Controller
{
    /**
    * UserService service object.
    *
    * @var UserService
    */
    protected $userService;
    
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Show Profile page for Logged in user
     *
     * @return View
     */
    public function edit(): View
    {
        return view('profile.edit',[
            'user' => Auth::user(),
            'company'=> Company::find(1),
            'roles'=> Role::all(),
        ]);
    }

    /**
     * Update User in Database
     *
     * @param UpdateUserFormRequest $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(UpdateUserFormRequest $request): RedirectResponse
    {
        $user = Auth::user();
        $result = $this->userService->updateUser($request->all(), $user);
        if ($result === true) {
            return redirect()->route('profile.edit');
        }

        return back()->withErrors($result->getMessage())->withInput();
    }
}
