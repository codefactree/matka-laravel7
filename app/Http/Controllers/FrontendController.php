<?php

namespace App\Http\Controllers;

use App\Services\CompanyService;
use App\Services\MarketService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FrontendController extends Controller
{
    /**
    * MarketService service object.
    *
    * @var MarketService
    */
    protected $marketService;
    
    /**
    * CompanyService service object.
    *
    * @var CompanyService
    */
    protected $companyService;
    
    public function __construct(
        MarketService $marketService,
        CompanyService $companyService
    ) {
        $this->marketService = $marketService;
        $this->companyService = $companyService;
    }


    public function index()
    {
        $market = $this->marketService->getAllMarketDetailsForApi();
        if($market instanceof Exception) { //if there's an exception while fetching Market data throw error
            return response()->json(errorResponse($market));
        }
        
        $company = $this->companyService->getCompanyDetails();
        if($company instanceof Exception) { //if there's an exception while fetching Company data throw error
            return response()->json(errorResponse($company));
        }
        
        return view('frontend.index', [
            'markets' => $market,
            'company' => $company
        ]);
    }
    
    public function terms()
    {
        return view('frontend.terms');
    }


    public function privacypolicy()
    {
        return view('frontend.privacypolicy');
    }
}
