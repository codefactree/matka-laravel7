<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\CreateFormRequest;
use App\Http\Requests\User\UpdateUserFormRequest;
use App\Model\Role;
use App\Model\User;
use App\Services\AccountStatementService;
use App\Services\UserService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
    * UserService service object.
    *
    * @var UserService
    */
    protected $userService;
   
    /**
    * AccountStatementService service object.
    *
    * @var AccountStatementService
    */
    protected $accountStatementService;
    
    public function __construct(UserService $userService, AccountStatementService $accountStatementService)
    {
        $this->userService = $userService;
        $this->accountStatementService = $accountStatementService;
    }
    
    /**
    * Display User Index page
    *
    * @return View
    */
    public function index(): View
    {
        return view('user.index');
    }
    
    /**
    * Get All Users from database
    *
    * @return JsonResponse
    */
    public function getAllUsers(): JsonResponse
    {
        return $this->userService->getAllUsers();
    }
    
    /**
    * Display User Index page
    *
    * @return View
    */
    public function create(): View
    {
        return view('user.create',[ 'roles' => Role::all()]);
    }
    
    /**
    * Store User in Database
    *
    * @param CreateFormRequest $request
    * @return RedirectResponse
    */
    public function store(CreateFormRequest $request): RedirectResponse
    {
        $result = $this->userService->saveUser($request->all());

        if ($result instanceof Exception) {
            return back()->withErrors($result->getMessage())->withInput();
        }
        
        return redirect()->route('user.index');
    }
    
    /**
    * Show User Detail for View Mode
    *
    * @param User $user
    * @return View
    */
    public function show(User $user): View
    {
        return view('user.view', ['user' => $user]);
    }

    /**
    * Show User Detail for Edit Mode
    *
    * @param User $user
    * @return View
    */
    public function edit(User $user): View
    {
        return view('user.edit', [
            'user' => $user,
            'roles' => Role::all()
        ]);
    }
   
    /**
     * Update User in Database
     *
     * @param UpdateUserFormRequest $request
     * @param User $user
     * @return RedirectResponse
     */
    public function update(UpdateUserFormRequest $request, User $user): RedirectResponse
    {
        $result = $this->userService->updateUser($request->all(), $user);
        if ($result === true) {
            return redirect()->route('user.index');
        }

        return back()->withErrors($result->getMessage())->withInput();
    }
        
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id): bool
    {
        return $this->userService->deleteUser($id);
    }

    /**
     * Add Money to user wallet
     *
     * @param Request $request
     * @param User $user
     * @return void
     */
    public function addMoneyToWallet(Request $request, User $user)
    {
        $request->validate([
            'amount' => 'required'
        ]);

        $this->userService->addMoneyToWallet($request->all(), $user);
        
        return $this->accountStatementService->addMoneyToWalletByAdmin($request->all(), $user);
    }

    /**
     * Load Account Statement Page
     *
     * @param User $user
     * @return View
     */
    public function accountStatement(User $user): View
    {
        return view('user.accountStatement', [
            'user' => $user
        ]);
    }
   
    /**
     * Load Account Statement Data
     *
     * @param User $user
     * @return JsonResponse
     */
    public function getAccountStatement(User $user): JsonResponse
    {
        return $this->accountStatementService->getAccountStatementForDatable($user);
    }
}
