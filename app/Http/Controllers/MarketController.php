<?php

namespace App\Http\Controllers;

use App\Http\Requests\Market\MarketEditFormRequest;
use App\Http\Requests\MarketSaveFormRequest;
use App\Model\Market;
use App\Services\MarketService;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class MarketController extends Controller
{
    /**
    * MarketService service object.
    *
    * @var MarketService
    */
    protected $marketService;
    
    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }
    
    /**
    * Display Market Index page
    *
    * @return View
    */
    public function index(): View
    {
        return view('market.index');
    }
    
    /**
    * Display Market Index page
    *
    * @return View
    */
    public function reorder(): View
    {
        return view('market.reorder',[
            'markets' => $this->marketService->getMarketLists()
        ]);
    }

    /**
    * Update the order of the Market
    *
    * @return RedirectResponse
    */
    public function updateReorder(Request $request): RedirectResponse
    {
        $result = $this->marketService->updateReorder($request->all());
        if($result instanceof Exception) {
            return back()->withErrors($result->getMessage());
        }
        return redirect()->route('market.index');
    }
    
    /**
    * Get All Markets from database
    *
    * @return JsonResponse
    */
    public function getAllMarkets(): JsonResponse
    {
        return $this->marketService->getAllMarkets();
    }
    
    /**
    * Save market n db
    *
    * @param MarketSaveFormRequest $request
    * @return void
    */
    public function store(MarketSaveFormRequest $request)
    {
        return $this->marketService->saveMarket($request->all());
    }
    
    /**
    * Show Market detail for View/Edit Modal Page
    *
    * @param Market $market
    * @return Market
    */
    public function show(Market $market): Market
    {
        return $market;
    }
    
    /**
    * Update market detail in Db
    *
    * @param MarketEditFormRequest $request
    * @param Market $market
    * @return boolean
    */
    public function update(MarketEditFormRequest $request, Market $market): bool
    {
        return $this->marketService->updateMarket($request->all(), $market);
    }
    
    /**
    * Soft Delete Market from Db
    *
    * @param Market $market
    * @return boolean
    */
    public function destroy(Market $market): bool
    {
        return $this->marketService->deleteMarket($market->id);
    }
}
