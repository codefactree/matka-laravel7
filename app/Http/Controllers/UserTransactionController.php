<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\UserTransactionService;

class UserTransactionController extends Controller
{
    /**
    * UserTransactionService service object.
    *
    * @var UserTransactionService
    */
    protected $userTransactionService;
    
    public function __construct(
        UserTransactionService $userTransactionService
    ) {
        $this->userTransactionService = $userTransactionService;
    }

    /**
     * Fetch all the Pending Withdraw request
     *
     * @return void
     */
    public function getPendingWithdrawRequest()
    {
        return $this->userTransactionService->getWithdrawData('pending');
    }

    /**
     * Change User withdraw request status to completed
     *
     * @param integer $transactionId
     * @return bool
     */
    public function changeWithdrawStatus(int $transactionId): bool
    {
        return $this->userTransactionService->changeWithdrawStatus($transactionId);
    }
}
