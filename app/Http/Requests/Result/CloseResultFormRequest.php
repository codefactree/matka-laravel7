<?php

namespace App\Http\Requests\Result;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CloseResultFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dateClose' => [
                'required',
                'date',
            ],
            'last3' => [ 'required', 'digits:3',  Rule::in(allResultValues())],
            'close_timeHidden' => [
                // new TodayTimeValidation($this->request->all())
            ],
            'mid2' => [
                'required',
                'digits:1'
            ],
        ];
    }

    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
    public function messages()
    {
        return [
            'dateClose.after' => 'Cannot enter previous day enteries',
            'last3.in_array' => 'Incorrect number in Close Panna result field',
        ];
    }
}
