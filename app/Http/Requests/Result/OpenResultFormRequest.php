<?php

namespace App\Http\Requests\Result;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OpenResultFormRequest extends FormRequest
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }
    
    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        return [
            'dateOpen' => [
                'required',
                'date',
            ],
            'open_timeHidden' => [
                // new TodayTimeValidation($this->request->all())
            ],
            'first3' => [ 'required', 'digits:3',  Rule::in(allResultValues())],
            'mid1' => [
                'required',
                'digits:1'
            ],
        ];
    }
    
    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
    public function messages()
    {
        return [
            'dateOpen.after' => 'Cannot enter previous day enteries',
            'first3.in_array' => 'Incorrect number in Open Panna result field',
        ];
    }
}
