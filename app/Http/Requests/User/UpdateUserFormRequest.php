<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UpdateUserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        if(Request::is('profile') || Request::is('api/profile')) {
            $id = Auth::user()->id;
        } else {
            $id = $this->user->id;
        }
        return [
            'name' => [
                'required'
            ],
            'user_name' => [
                'required', 
                'unique:users,user_name,'.$id.',id,deleted_at,NULL'
            ],
            'email' =>[
                'email',
                'unique:users,email,'.$id.',id,deleted_at,NULL'
            ],
            'mobile_no' =>[
                'required',
                'string',
                'unique:users,mobile_no,'.$id.',id,deleted_at,NULL'
            ],
            'wallet_amount' => 'integer',
        ];
    }
}
