<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class CreateFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name' => [
                'required'
            ],
            'user_name' => [
                'required', 
                'unique:users,user_name,NULL,id,deleted_at,NULL'
            ],
            'email' =>[
                'email',
                'unique:users,email,NULL,id,deleted_at,NULL'
            ],
            'mobile_no' =>[
                'required',
                'integer',
                'unique:users,mobile_no,NULL,id,deleted_at,NULL'
            ],
            'password' => 'required',
            'wallet_amount' => 'integer',
        ];
    }
}
