<?php

use Carbon\Carbon;

if (!function_exists('getTodayDateYMD')) {
    
    /**
     * Today Date in Y-M-D format
     *
     * @param Exception $exception
     * 
     * @return string a string in human readable format
     * */
    function getTodayDateYMD():string
    {
        return Carbon::now()->format('Y-m-d');
    }
}

if (!function_exists('convertYMDtoDMY')) {
    
    /**
     * Today Date in Y-M-D format
     *
     * @param Exception $exception
     * 
     * @return string a string in human readable format
     * */
    function convertYMDtoDMY($date)
    {
        return date("d-m-Y", strtotime($date));
    }
}

if (!function_exists('convertDMYtoYMD')) {
    
    /**
     * Today Date in Y-M-D format
     *
     * @param Exception $exception
     * 
     * @return string a string in human readable format
     * */
    function convertDMYtoYMD($date)
    {
        return date("Y-m-d", strtotime($date));
    }
}

if (!function_exists('errorResponse')) {
    /**
     * Returns a Response array for Error
     *
     * @param Exception $exception
     * 
     * @return array a string in human readable format
     * */
    function errorResponse(Exception $exception):array
    {
        return [
            'status' => ($exception->getCode() == 0) ? 500 : $exception->getCode(),
            'message' => $exception->getMessage()
        ];
    }
}

if (!function_exists('generic_error_response')) {
    /**
     * Get error response array
     *
     * @param string $message
     * @return array
     */
    function generic_error_response(string $message): array
    {
        return [
            'status' => 500,
            'message' => $message
        ];
    }
}

if (!function_exists('successResponse')) {
    /**
     * Returns a Response array for Error
     *
     * @param array $data
     * 
     * @return array in human readable format
     * */
    function successResponse(array $data):array
    {
        return [
            'status' => 200,
            'body' => $data
        ];
    }
}

if (!function_exists('allResultValues')) {
    /**
     * Returns a Response array for Error
     *
     * @return array a string in human readable format
     * */
    function allResultValues():array
    {
        return array_merge(config('constant.single_patti'), config('constant.double_patti'), config('constant.tripple_patti'));
    }
}