<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Iatstuti\Database\Support\CascadeSoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes, HasRoles, CascadeSoftDeletes;

    protected $cascadeDeletes = ['userDetail', 'userBids', 'userBidDetails', 'accountSatements', 'withDrawRequests'];
    
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'role_id', 'name', 'email', 'password', 'user_name', 'mobile_no', 'fcm_token', 'location', 'wallet_amount'
    ];
    
    /**
    * The attributes that should be hidden for arrays.
    *
    * @var array
    */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
    * The attributes that should be cast to native types.
    *
    * @var array
    */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    /**
    * User BelongsTo Role
    * 
    * @return BelongsTo
    */
    function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }
    
    /**
    * User HasOne realationship with UserDetail
    * 
    * @return HasOne
    */
    function userDetail(): HasOne
    {
        return $this->hasOne(UserDetail::class);
    }
    
    /**
    * User HasOne realationship with UserDetail
    * 
    * @return HasOne
    */
    function userBids(): HasMany
    {
        return $this->hasMany(UserBid::class);
    }

    /**
    * User HasOne realationship with UserDetail
    * 
    * @return HasOne
    */
    function userBidDetails(): HasMany
    {
        return $this->hasMany(UserBidDetail::class);
    }

    /**
    * User HasOne realationship with UserDetail
    * 
    * @return HasOne
    */
    function accountSatements(): HasMany
    {
        return $this->hasMany(AccountStatement::class);
    }

    /**
    * User HasOne realationship with UserDetail
    * 
    * @return HasOne
    */
    function withDrawRequests(): HasMany
    {
        return $this->hasMany(WithdrawRequest::class);
    }
}
