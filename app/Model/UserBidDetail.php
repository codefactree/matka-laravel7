<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBidDetail extends Model
{
    use SoftDeletes;

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'user_bid_id', 'user_id', 'bidding_no', 'amount'
    ];

    /**
     * UserDetail BelongsTo User
     * 
     * @return BelongsTo
     */
    function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * UserDetail BelongsTo User
     * 
     * @return BelongsTo
     */
    function user_bid(): BelongsTo
    {
        return $this->belongsTo(UserBid::class);
    }
}
