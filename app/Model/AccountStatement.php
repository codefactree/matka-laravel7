<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class AccountStatement extends Model
{
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'user_id', 'date', 'particulars', 'points', 'balance'
    ];
    
    /**
    * UserDetail BelongsTo User
    * 
    * @return BelongsTo
    */
    function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
