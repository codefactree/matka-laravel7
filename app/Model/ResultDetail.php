<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ResultDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'result_id', 'market_id', 'game_id', 'date', 'total_amount', 'is_money_transfered'
    ];

    function result(): BelongsTo
    {
        return $this->belongsTo(Result::class);
    }

    function resultUserDetail(): HasMany
    {
        return $this->hasMany(ResultUserDetail::class,'result_details_id','id');
    }
    
    /**
     * ResultDetails BelongsTo Market
     * 
     * @return BelongsTo
     */
    function market(): BelongsTo
    {
        return $this->belongsTo(Market::class);
    }

    /**
     * ResultDetails BelongsTo Market
     * 
     * @return BelongsTo
     */
    function game(): BelongsTo
    {
        return $this->belongsTo(Game::class);
    }
}
