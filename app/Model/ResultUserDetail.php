<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ResultUserDetail extends Model
{
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'result_details_id', 'user_id', 'bidding_no', 'amount'
    ];
    
    function resultDetail(): BelongsTo
    {
        return $this->belongsTo(ResultDetail::class, 'result_details_id', 'id');
    }
    
    /**
    * UserDetail BelongsTo User
    * 
    * @return BelongsTo
    */
    function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
