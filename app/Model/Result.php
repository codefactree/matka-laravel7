<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Result extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'market_id', 'date', 'first3', 'mid1', 'mid2', 'last3', 'is_open_result_generated', 'is_close_result_generated'
    ];

    /**
     * Result BelongsTo Market
     * 
     * @return BelongsTo
     */
    function market(): BelongsTo
    {
        return $this->belongsTo(Market::class);
    }

    /**
     * Result has many details as per the game
     *
     * @return HasMany
     */
    function resultDetails(): HasMany
    {
        return $this->hasMany(ResultDetail::class);
    }
}
