<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class Market extends Model
{
    use SoftDeletes;
    
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name', 'open_time', 'close_time', 'weekly_config'
    ];
    
    /**
    * Market HasMany Results
    * 
    * @return HasMany|HasOne
    */
    public function result($date = null)
    {
        if($date != null) {
            return $this->hasOne(Result::class)->where('date', $date);
        }
        
        return $this->hasMany(Result::class);
    }
}
