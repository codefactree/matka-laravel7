<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserBid extends Model
{
    use SoftDeletes;
    
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'user_id', 'game_type', 'game_id', 'market_id', 'date', 'total_amount'
    ];
    
    /**
     * UserDetail BelongsTo User
     * 
     * @return BelongsTo
     */
    function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * MarketId BelongsTo Market
     * 
     * @return BelongsTo
     */
    function market(): BelongsTo
    {
        return $this->belongsTo(Market::class);
    }

    /**
    * User HasMany realationship with UserBidDetail
    * 
    * @return HasMany
    */
    function userBidDetail(): HasMany
    {
        return $this->hasMany(UserBidDetail::class);
    }

    /**
     * game_id BelongsTo Game
     * 
     * @return BelongsTo
     */
    function game(): BelongsTo
    {
        return $this->belongsTo(Game::class);
    }
}
