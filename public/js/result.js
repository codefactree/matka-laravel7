$(function () {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
    
    //Initialize Select2 Elements
    $('.select2').select2()
    
    /**
    * Time Pickers
    */
    $('#resultDate').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD-MM-YYYY'
        },
    });
    loadResultTable();
    /**
    * On result Date change
    */
    $('#resultDate').on('change', function()
    {
        loadResultTable();
    });
    /**
    * Time Pickers
    */
    $('#dateOpen').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD-MM-YYYY'
        },
        // minDate: new Date()
    });
    /**
    * Time Pickers
    */
    $('#dateClose').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD-MM-YYYY'
        },
        // minDate: new Date()
    });
    
    /**
    * Calculate 4th digit
    */
    $('#first3').on('change', function()
    {
        if(this.value.length === 3){
            var digit = getSecondDigit(this.value);
            $('#fourth_number').val(digit);
            $('#mid1').val(digit);
        } 
    });
    
    /**
    * Calculate 5th digit
    */
    $('#last3').on('change', function()
    {
        if(this.value.length === 3){
            var digit = getSecondDigit(this.value);
            $('#fifth_number').val(digit);
            $('#mid2').val(digit);
        } 
    });
    
    /**
    * Calculate Sum of all the number the individual number 
    * and give 2nd digit number
    */
    function getSecondDigit(value){
        var sumofNumber = sum(value.split(""));
        if(sumofNumber > 9) {
            sumofNumber = sumofNumber.toString().split("")[1];
        }
        
        return sumofNumber
    }
    
    /**
    * Get Sum of array  
    */
    function sum(input){
        if (toString.call(input) !== "[object Array]")
        return false;
        
        var total =  0;
        for(var i=0;i<input.length;i++)
        {                  
            if(isNaN(input[i])){
                continue;
            }
            total += Number(input[i]);
        }
        return total;
    }
    
    
    function loadResultTable() {
        date = ($('#resultDate').val());
        formatted_date = moment(date, "DD-MM-YYYY").format("YYYY-MM-DD");
        
        /**
        * Result index page
        * load data table
        */
        $("#resultDataTable").DataTable({
            "processing": true,
            "serverSide": true,
            "autoWidth": false,
            "ajax":{
                "url" :  "/admin/getAllResult/"+formatted_date,
                "type" :  "GET",
            },
            "columnDefs": [
                { width: 10, targets: 0 },
                { width: 80, targets: 1 },
                { width: 20, targets: 2 },
                { width: 60, targets: 3 },
                { width: 20, targets: 4 },
                { width: 60, targets: 5 },
                { width: 20, targets: 6 },
            ],
            "columns": [
                { 'data': 'DT_RowIndex', orderable: false, searchable: false},
                { "data": "name" },
                { "data": "open_time" },
                { "data": "open_action", "orderable": false, "searchable": false},
                { "data": "close_time"},
                { "data": "close_action", "orderable": false, "searchable": false},
                { "data": "details_action", "orderable": false, "searchable": false}
            ],
            "bDestroy": true
        });
    };
    
    /**
    * View Button Clicked From User Data Table
    * Load User Data in Modal
    */
    $(document).on('click', '#viewOpenModalButton', function(event) {
        var id = $(this).attr('data-id');
        $.ajax({
            url: "/admin/result/market/"+ id,
            method: 'get',
            success: function(data){
                $('.alert-danger').hide();
                $('#nameOpenView').val(data.market.name);
                $('#open_timeView').val(moment(data.market.open_time, ["HH:mm"]).format("hh:mm a"));
                if(data.result != null) {
                    $('#dateOpenView').val(moment(data.result.date).format("DD-MM-YYYY"));
                    $('#first3View').val(data.result.first3);
                    $('#fourth_numberView').val(data.result.mid1);
                }
                $("#viewOpenActionModal").modal('show');
            },
            error: function (reject) {
                Toast.fire({
                    icon: 'warning',
                    title: "Oops there's an error while Loading User data"
                })
            }
        });
    });
    
    /**
    * Open MOdal for User to Enter values for first 3 digit for open_time
    * Load User Data in Modal
    */
    $(document).on('click', '#openEditModalButton', function(event) {
        var id = $(this).attr('data-id');
        $.ajax({
            url: "/admin/market/"+ id,
            method: 'get',
            success: function(result){
                $('#marketIdOpen').val(result.id);
                $('#nameOpen').val(result.name);
                $('#open_time').val(moment(result.open_time, ["HH:mm"]).format("hh:mm a"));
                $('#open_timeHidden').val(moment(result.open_time, ["HH:mm"]).format("hh:mm a"));
            },
            error: function (reject) {
                Toast.fire({
                    icon: 'warning',
                    title: "Oops there's an error while Loading User data"
                })
            }
        });
        
        $.ajax({
            url: "/admin/result/market/"+ id,
            method: 'get',
            success: function(data){
                $('.alert-danger').hide();
                if(data.result != null) {
                    console.log(data.result.first3);
                    $('#dateOpen').val(moment(data.result.date).format("DD-MM-YYYY"));
                    $('#first3').val(data.result.first3);
                    $('#fourth_number').val(data.result.mid1);
                    $('#mid1').val(data.result.mid1);
                }
                $("#openActionModal").modal('show');
            },
            error: function (reject) {
                Toast.fire({
                    icon: 'warning',
                    title: "Oops there's an error while Loading User data"
                })
            }
        });
    });
    
    /**
    * Validate the Form in User create File
    */
    $('#openActionForm').validate({
        rules: {
            dateOpen: {
                required: true,
            },
            first3: {
                required: true,
                number: true
            },
            mid1: {
                required: true,
                minlength: 1,
                maxlength: 1,
                number: true
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
    
    /**
    * On submit of save button
    * Submit form for saving market in database
    */
    $('#saveOpenResult').click(function(e){
        e.preventDefault();
        if($('#openActionForm').valid()) {
            $.ajax({
                url: "/admin/result/open",
                method: 'post',
                data: $("#openActionForm").serialize(),
                success: function(result){
                    $('#openActionForm').trigger("reset"); //clear modal form data
                    $("#openActionModal").modal('hide'); // hide the modal
                    
                    Toast.fire({
                        icon: 'success',
                        title: 'Open Number Entered Successfully'
                    })
                },
                error: function (reject) {
                    $('.alert-danger').html('');
                    $.each(reject.responseJSON.errors, function(key, value){
                        $('.alert-danger').show();
                        $('.alert-danger').append('<li>'+value+'</li>');
                    });
                }
            });
        }
    });
    
    /**
    * Delete Button Clicked From Market Data Table
    */
    $(document).on('click', '#deleteOpenModalButton', function(event) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var id = $(this).attr('data-id');
                $.ajax({
                    url: "/admin/result/delete/open/"+ id,
                    method: 'PUT',
                    success: function(result){
                        Swal.fire(
                            'Deleted!',
                            'Todays Result for Open has been deleted.',
                            'success'
                            )
                        },
                        error: function (reject) {
                            Toast.fire({
                                icon: 'warning',
                                title: "Oops there's an error while Loading User data"
                            })
                        }
                    });
                }
            })
        });
        
        /**
        * Delete Button Clicked From Market Data Table
        */
        $(document).on('click', '#generateOpenResultButton', function(event) {
            Swal.fire({
                title: 'Generate Result for',
                text: "Single, Single Patti, Double Patti, Tripple Patti",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, generate it!'
            }).then((result) => {
                if (result.value) {
                    var id = $(this).attr('data-id');
                    $.ajax({
                        url: "/admin/result/generate/open/"+ id,
                        method: 'POST',
                        success: function(result){
                            Swal.fire(
                                'Generated!',
                                'Todays Result for Open has been Generated.',
                                'success'
                                )
                            },
                            error: function (reject) {
                                Toast.fire({
                                    icon: 'warning',
                                    title: "Oops there's an error while Loading User data"
                                })
                            }
                        });
                    }
                })
            });
            
            /**
            * Close Action Section 
            */
            
            /**
            * View Button Clicked From User Data Table
            * Load User Data in Modal
            */
            $(document).on('click', '#viewCloseModalButton', function(event) {
                var id = $(this).attr('data-id');
                $.ajax({
                    url: "/admin/result/market/"+ id,
                    method: 'get',
                    success: function(data){
                        $('.alert-danger').hide();
                        $('#nameCloseView').val(data.market.name);
                        $('#close_timeView').val(moment(data.market.close_time, ["HH:mm"]).format("hh:mm a"));
                        if(data.result!= null) {
                            $('#dateCloseView').val(moment(data.result.date).format("DD-MM-YYYY"));
                            $('#last3View').val(data.result.last3);
                            $('#fifth_numberView').val(data.result.mid2);
                        }
                        $("#viewCloseActionModal").modal('show');
                    },
                    error: function (reject) {
                        Toast.fire({
                            icon: 'warning',
                            title: "Oops there's an error while Loading User data"
                        })
                    }
                });
            });
            
            /**
            * Open Modal for User to Enter values for first 3 digit for open_time
            * Load User Data in Modal
            */
            $(document).on('click', '#editCloseModalButton', function(event) {
                var id = $(this).attr('data-id');
                $.ajax({
                    url: "/admin/market/"+ id,
                    method: 'get',
                    success: function(result){
                        $('#marketIdClose').val(result.id);
                        $('#nameClose').val(result.name);
                        $('#close_time').val(moment(result.close_time, ["HH:mm"]).format("hh:mm a"));
                        $('#close_timeHidden').val(moment(result.close_time, ["HH:mm"]).format("hh:mm a"));
                    },
                    error: function (reject) {
                        Toast.fire({
                            icon: 'warning',
                            title: "Oops there's an error while Loading User data"
                        })
                    }
                });
                
                $.ajax({
                    url: "/admin/result/market/"+ id,
                    method: 'get',
                    success: function(data){
                        $('.alert-danger').hide();
                        if(data.result != null) {
                            $('#dateClose').val(moment(data.result.date).format("DD-MM-YYYY"));
                            $('#last3').val(data.result.last3);
                            $('#fifth_number').val(data.result.mid2);
                            $('#mid2').val(data.result.mid2);
                        }
                        $("#closeActionModal").modal('show');
                    },
                    error: function (reject) {
                        Toast.fire({
                            icon: 'warning',
                            title: "Oops there's an error while Loading User data"
                        })
                    }
                });
            });
            
            /**
            * Validate the Form in User create File
            */
            $('#closeActionForm').validate({
                rules: {
                    dateClose: {
                        required: true,
                    },
                    last3: {
                        required: true,
                        number: true
                    },
                    mid2: {
                        required: true,
                        minlength: 1,
                        maxlength: 1,
                        number: true
                    },
                },
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
            
            /**
            * On submit of save button
            * Submit form for saving market in database
            */
            $('#saveCloseResult').click(function(e){
                e.preventDefault();
                if($('#closeActionForm').valid()) {
                    $.ajax({
                        url: "/admin/result/close",
                        method: 'post',
                        data: $("#closeActionForm").serialize(),
                        success: function(result){
                            $('#closeActionForm').trigger("reset"); //clear modal form data
                            $("#closeActionModal").modal('hide'); // hide the modal
                            if(result.status == 500) {
                                Toast.fire({
                                    icon: 'warning',
                                    title: result.message
                                })
                            } else {
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Close Number Entered Successfully'
                                })
                            }
                        },
                        error: function (reject) {
                            $('.alert-danger').html('');
                            $.each(reject.responseJSON.errors, function(key, value){
                                $('.alert-danger').show();
                                $('.alert-danger').append('<li>'+value+'</li>');
                            });
                        }
                    });
                }
            });
            
            /**
            * Delete Button Clicked From Market Data Table
            */
            $(document).on('click', '#deleteCloseModalButton', function(event) {
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        var id = $(this).attr('data-id');
                        $.ajax({
                            url: "/admin/result/delete/close/"+ id,
                            method: 'PUT',
                            success: function(result){
                                Swal.fire(
                                    'Deleted!',
                                    'Todays Result for Close has been deleted.',
                                    'success'
                                    )
                                },
                                error: function (reject) {
                                    Toast.fire({
                                        icon: 'warning',
                                        title: "Oops there's an error while Loading User data"
                                    })
                                }
                            });
                        }
                    })
                });
            });