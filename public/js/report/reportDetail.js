$(function () {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
    
    /**
    * Time Pickers
    */
    $('#reportDate').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD-MM-YYYY'
        },
    });
   
    $( "#captureButton" ).click(function() {
        html2canvas(document.querySelector('#capture'), 
        {
            x: 0,
            width: window.outerWidth + window.innerWidth,
            windowWidth: window.outerWidth + window.innerWidth,
        }
        ).then(function(canvas) {
            saveAs(canvas.toDataURL(), $('#marketName').val()+'.png');
        });
    });


    function saveAs(uri, filename) {

        var link = document.createElement('a');
    
        if (typeof link.download === 'string') {
    
            link.href = uri;
            link.download = filename;
    
            //Firefox requires the link to be in the body
            document.body.appendChild(link);
    
            //simulate click
            link.click();
    
            //remove the link when done
            document.body.removeChild(link);
    
        } else {
    
            window.open(uri);
    
        }
    }
    

});