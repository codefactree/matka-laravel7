$(function () {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
    
    /**
    * Time Pickers
    */
    $('#openTimePicker').datetimepicker({
        format: 'LT'
    });
    $('#closeTimePicker').datetimepicker({
        format: 'LT'
    });
    $('#openTimePickerEdit').datetimepicker({
        format: 'LT'
    });
    $('#closeTimePickerEdit').datetimepicker({
        format: 'LT'
    });
    
    /**
    * Market index page
    * load data table
    */
    $("#marketDataTable").DataTable({
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        "ajax":{
            "url" :  "/admin/getAllMarkets",
            "type" :  "GET",
        },
        "columns": [
        { 'data': 'DT_RowIndex', orderable: false, searchable: false},
        { "data": "name" },
        { "data": "open_time" },
        { "data": "close_time"},
        { "data": "action", "orderable": false, "searchable": false}
        ]
    });
    
    /**
    * View Button Clicked From User Data Table
    * Load User Data in Modal
    */
    $(document).on('click', '#viewModalButton', function(event) {
        var id = $(this).attr('data-id');
        $.ajax({
            url: "/admin/market/"+ id,
            method: 'get',
            success: function(result){
                fillmodalData(result, 'View')
                $("#viewModal").modal('show');
            },
            error: function (reject) {
                Toast.fire({
                    icon: 'warning',
                    title: "Oops there's an error while Loading User data"
                })
            }
        });
    });
    
    /**
    * Validate the Form in User create File
    */
    $('#marketForm').validate({
        rules: {
            name: {
                required: true,
            },
            openTimePicker: {
                required: true,
            },
            closeTimePicker: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
    
    /**
    * On submit of save button
    * Submit form for saving market in database
    */
    $('#saveMarket').click(function(e){
        e.preventDefault();
        if($('#marketForm').valid()) {
            $.ajax({
                url: "/admin/market",
                method: 'post',
                data: $("#marketForm").serialize(),
                success: function(result){
                    $('#marketForm').trigger("reset"); //clear modal form data
                    $("#createModal").modal('hide'); // hide the modal
                    $("#marketDataTable").DataTable().ajax.reload(); // reload the data table on index page
                    
                    Toast.fire({
                        icon: 'success',
                        title: 'Market Saved Successfully'
                    })
                },
                error: function (reject) {
                    $('.alert-danger').html('');
                    $.each(reject.responseJSON.errors, function(key, value){
                        $('.alert-danger').show();
                        $('.alert-danger').append('<li>'+value+'</li>');
                    });
                }
            });
        }
    });
    
    /**
    * View Button Clicked From User Data Table
    * Load User Data in Modal
    */
    $(document).on('click', '#editModalButton', function(event) {
        var id = $(this).attr('data-id');
        $.ajax({
            url: "/admin/market/"+ id,
            method: 'get',
            success: function(result){
                fillmodalData(result, 'Edit')
                $("#editModal").modal('show');
                
            },
            error: function (reject) {
                Toast.fire({
                    icon: 'warning',
                    title: "Oops there's an error while Loading User data"
                })
            }
        });
    });
    
    /**
    * Validate the Form in User create File
    */
    $('#marketEditForm').validate({
        rules: {
            nameEdit: {
                required: true,
            },
            openTimePickerEdit: {
                required: true,
            },
            closeTimePickerEdit: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
    
    /**
    * On submit of save button
    * Submit form for saving user in database
    */
    $('#updateMarket').click(function(event){
        event.preventDefault();
        var id = $('#id').val();
        if($('#marketEditForm').valid()) {
            $.ajax({
                url: "/admin/market/"+ id,
                method: 'PUT',
                data: $("#marketEditForm").serialize(),
                success: function(result){
                    $('#marketEditForm').trigger("reset"); //clear modal form data
                    $("#editModal").modal('hide'); // hide the modal
                    $("#marketDataTable").DataTable().ajax.reload(); // reload the data table on index page
                    
                    Toast.fire({
                        icon: 'success',
                        title: 'Market Updated Successfully'
                    })
                },
                error: function (reject) {
                    $('.alert-danger').html('');
                    $.each(reject.responseJSON.errors, function(key, value){
                        $('.alert-danger').show();
                        $('.alert-danger').append('<li>'+value+'</li>');
                    });
                }
            });
        }
    });
    
    /**
    * Fill the Modal Form with the data
    */
    function fillmodalData(data,mode) {
        $('.alert-danger').hide();
        $('#name'+mode).val(data.name);
        $('#openTimePicker'+mode).val(moment(data.open_time, ["HH:mm"]).format("hh:mm a"));
        $('#closeTimePicker'+mode).val(moment(data.close_time, ["HH:mm"]).format("hh:mm a"));
        let weeks= JSON.parse(data.weekly_config);
        Object.keys(weeks).forEach(element => {
            if(weeks[element] === 1) {
                $('#'+element+mode).prop( "checked", true );
            }
        });
        if(mode == "Edit") {
            $('#id').val(data.id);
        }
    }
    
    /**
    * Delete Button Clicked From Market Data Table
    */
    $(document).on('click', '#deleteModalButton', function(event) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var id = $(this).attr('data-id');
                $.ajax({
                    url: "/admin/market/"+ id,
                    method: 'DELETE',
                    success: function(result){
                        $("#marketDataTable").DataTable().ajax.reload(); // reload the data table on index page
                        Swal.fire(
                        'Deleted!',
                        'Market has been deleted.',
                        'success'
                        )
                    },
                    error: function (reject) {
                        Toast.fire({
                            icon: 'warning',
                            title: "Oops there's an error while Loading User data"
                        })
                    }
                });
            }
        })
    });
});