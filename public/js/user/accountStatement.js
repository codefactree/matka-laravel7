$(function () {
    
    let id = $('#userId').val();
    
    /**
    * User index page
    * load data table
    */
    $("#accountStatementDataTable").DataTable({
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        "ajax":{
            "url" :  "/admin/getAccountStatement/"+ id,
            "type" :  "GET",
        },
        "columns": [
        { 'data': 'DT_RowIndex', orderable: false, searchable: false},
        { "data": "date" },
        { "data": "particulars" },
        { "data": "points"},
        { "data": "balance" },
        ]
    });
});