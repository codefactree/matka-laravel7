$(function () {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
    
    /**
    * User index page
    * load data table
    */
    $("#userDataTable").DataTable({
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        "ajax":{
            "url" :  "/admin/getAllUsers",
            "type" :  "GET",
        },
        "columnDefs": [
            { width: 10, targets: 0 },
            { width: 50, targets: 1 },
            { width: 200, targets: 2 },
            { width: 100, targets: 3 },
            { width: 70, targets: 4 },
            { width: 10, targets: 5 },
            { width: 160, targets: 6 },
        ],
        "columns": [
        { 'data': 'DT_RowIndex', orderable: false, searchable: false},
        { "data": "name" },
        { "data": "email" },
        { "data": "user_name"},
        { "data": "mobile_no" },
        { "data": "wallet_amount" },
        { "data": "action", "orderable": false, "searchable": false}
        ]
    });
    
    /**
    * Delete Button Clicked From User Data Table
    */
    $(document).on('click', '#deleteModalButton', function(event) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var id = $(this).attr('data-id');
                $.ajax({
                    url: "/admin/user/"+ id,
                    method: 'DELETE',
                    success: function(result){
                        $("#userDataTable").DataTable().ajax.reload(); // reload the data table on index page
                        Swal.fire(
                        'Deleted!',
                        'User has been deleted.',
                        'success'
                        )
                    },
                    error: function (reject) {
                        Toast.fire({
                            icon: 'warning',
                            title: "Oops there's an error while Loading User data"
                        })
                    }
                });
            }
        })
    });


    /**
    * View Button Clicked From User Data Table
    * Load User Data in Modal
    */
    $(document).on('click', '#walletModalButton', function(event) {
        $('#id').val($(this).attr('data-id'));
        $("#walletModal").modal('show');
    });

    /**
    * Validate the Form in User create File
    */
    $('#walletEditForm').validate({
        rules: {
            amount: {
                required: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });

    /**
    * On submit of save button
    * Submit form for saving user in database
    */
    $('#updateWallet').click(function(event){
        event.preventDefault();
        var id = $('#id').val();
        if($('#walletEditForm').valid()) {
            $.ajax({
                url: "/admin/wallet/addMoney/"+ id,
                method: 'PUT',
                data: $("#walletEditForm").serialize(),
                success: function(result){
                    $('#walletEditForm').trigger("reset"); //clear modal form data
                    $("#walletModal").modal('hide'); // hide the modal
                    $("#userDataTable").DataTable().ajax.reload(); // reload the data table on index page
                    
                    Toast.fire({
                        icon: 'success',
                        title: 'Wallet Updated Successfully'
                    })
                },
                error: function (reject) {
                    $('.alert-danger').html('');
                    $.each(reject.responseJSON.errors, function(key, value){
                        $('.alert-danger').show();
                        $('.alert-danger').append('<li>'+value+'</li>');
                    });
                }
            });
        }
    });
});