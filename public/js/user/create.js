$(function () {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
    
    /**
    * Validate the Form in User create File
    */
    $('#userCreateForm').validate({
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            user_name: {
                required: true,
            },
            password: {
                required: true,
                minlength: 5
            },
            mobile_no: {
                required: true,
                digits: true,
            },
            wallet_amount: {
                required: true,
                digits: true,
            },
        },
        messages: {
            email: {
                required: "Please enter a email address",
                email: "Please enter a vaild email address"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
    
    /**
    * On submit of save button
    * Submit form for saving user in database
    */
    $('#saveUser').click(function(e){
        e.preventDefault();
        if($('#userCreateForm').valid()) {
            $('#userCreateForm').submit();
        }
    });
});