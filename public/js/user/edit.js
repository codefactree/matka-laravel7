$(function () {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
    
    /**
    * Validate the Form in User create File
    */
    $('#userUpdateForm').validate({
        rules: {
            name: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            user_name: {
                required: true,
            },
            mobile_no: {
                required: true,
                digits: true,
            },
            wallet_amount: {
                required: true,
                digits: true,
            },
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
    
    /**
    * On submit of save button
    * Submit form for saving user in database
    */
    $('#updateUser').click(function(event){
        event.preventDefault();

        if($('#userUpdateForm').valid()) {
            $('#userUpdateForm').submit();
        }
    });
});