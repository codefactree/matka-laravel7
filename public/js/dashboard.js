$(function () {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
    
    /**
    * Withdraw index page
    * load data table
    */
    $("#withdrawDataTable").DataTable({
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        "ajax":{
            "url" :  "/admin/getPendingWithdrawRequest",
            "type" :  "GET",
        },
        "columns": [
        { 'data': 'DT_RowIndex', orderable: false, searchable: false},
        { "data": "user_name" },
        { "data": "created_at" },
        { "data": "amount"},
        { "data": "action", "orderable": false, "searchable": false}
        ]
    });
    
    /**
    * Delete Button Clicked From Market Data Table
    */
    $(document).on('click', '#changeWithDrawStatus', function(event) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You want to change the Withdraw status !",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes'
        }).then((result) => {
            if (result.value) {
                var id = $(this).attr('data-id');
                $.ajax({
                    url: "/admin/wallet/withDraw/changeStatus/"+ id,
                    method: 'PUT',
                    success: function(result){
                        $("#withdrawDataTable").DataTable().ajax.reload(); // reload the data table on index page
                        Swal.fire(
                        'Status Changed!',
                        'Withdraw status has been changed.',
                        'success'
                        )
                    },
                    error: function (reject) {
                        Toast.fire({
                            icon: 'warning',
                            title: "Oops there's an error while Loading User data"
                        })
                    }
                });
            }
        })
    });
});