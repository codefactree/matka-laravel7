$(function () {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })
    
    /**
    * Time Pickers
    */
    $('#reportDate').daterangepicker({
        singleDatePicker: true,
        locale: {
            format: 'DD-MM-YYYY'
        },
    });

   
    loadReportDataTable();
    totalBidAmount();
    
    $('#reportDate').change(function() {
        loadReportDataTable();
        totalBidAmount()
    });


    function loadReportDataTable() {
        date = ($('#reportDate').val());
        formatted_date = moment(date, "DD-MM-YYYY").format("YYYY-MM-DD");

        /**
        * Market index page
        * load data table
        */
        $("#reportDataTable").DataTable({
            "processing": true,
            "serverSide": true,
            "autoWidth": false,
            "ajax":{
                "url" :  "/admin/getReport/"+formatted_date,
                "type" :  "GET",
            },
            "columns": [
                { "data": "market_id", "visible": false },
                { 'data': 'DT_RowIndex', orderable: false, searchable: false},
                { "data": "name"},
                { "data": "game_type", "searchable": false},
                { "data": "total", "searchable": false},
                { "data": "action", "searchable": false},
            ],
            "bDestroy": true
        });
    }

    function totalBidAmount() {
        date = ($('#reportDate').val());
        formatted_date = moment(date, "DD-MM-YYYY").format("YYYY-MM-DD");

        $.ajax({
            url: "/admin/report/total-bid/"+ formatted_date,
            method: 'get',
            success: function(data){
                $('#total_amount').val(data);
            },
            error: function (reject) {
                Toast.fire({
                    icon: 'warning',
                    title: "Oops there's an error while Calculating Total Bid amount"
                })
            }
        });
    }
});