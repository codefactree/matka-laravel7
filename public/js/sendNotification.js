$(function () {
    /**
    * Validate the Form in Send Notification Form Create File
    */
    $('#sendNotificationForm').validate({
        rules: {
            title: {
                required: true,
            },
            body: {
                required: true,
            }
        },
        errorElement: 'span',
        errorPlacement: function (error, element) {
            error.addClass('invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function (element, errorClass, validClass) {
            $(element).addClass('is-invalid');
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).removeClass('is-invalid');
        }
    });
});