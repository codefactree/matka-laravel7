$(function () {
    /**
     * Get result detail for Market i.e. market result number history detail
     */
    $("#withDrawListDataTable").DataTable({
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        "ajax":{
            "url" : "getWithdrawList",
            "type" :  "GET",
        },
        "order": [[ 5, "desc" ]],
        "columnDefs": [
            { width: 10, targets: 0 },
            { width: 80, targets: 1 },
            { width: 20, targets: 2 },
        ],
        "columns": [
            { 'data': 'DT_RowIndex', orderable: false, searchable: false},
            { "data": "user_name" },
            { "data": "amount" },
            { "data": "status" },
            { "data": "success_date" },
            { "data": "created_at" },
        ],
        "bDestroy": true
    });

});