$(function () {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        onOpen: (toast) => {
            toast.addEventListener('mouseenter', Swal.stopTimer)
            toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    /**
    * Transfer Money
    */
    $(document).on('click', '#transferMoneyButton', function(event) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Transfer Money to User!'
        }).then((result) => {
            if (result.value) {
                var id = $(this).attr('data-id');
                $.ajax({
                    url: "/admin/result/transfer-money/"+ id,
                    method: 'POST',
                    success: function(result){
                        location.reload();
                        Swal.fire(
                            'Success!',
                            'Money Transfered Successfully.',
                            'success'
                            )
                    },
                    error: function (reject) {
                        Toast.fire({
                            icon: 'warning',
                            title: "Oops there's an error while Transfering Money"
                        })
                    }
                });
            }
        })
    });

    /**
     * Get result detail for Market i.e. market result number history detail
     */
    $("#resultDetailDataTable").DataTable({
        "processing": true,
        "serverSide": true,
        "autoWidth": false,
        "ajax":{
            "url" :  "/admin/result/getDetails/"+ $("#marketId").val(),
            "type" :  "GET",
        },
        "columnDefs": [
            { width: 10, targets: 0 },
            { width: 80, targets: 1 },
            { width: 20, targets: 2 }
        ],
        "columns": [
            { 'data': 'DT_RowIndex', orderable: false, searchable: false},
            { "data": "date" },
            { "data": "number", "orderable": false, "searchable": false },
        ],
        "bDestroy": true
    });

});