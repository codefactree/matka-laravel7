<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Sai Matka</title>
    
    {{-- Include all CSS --}}
    @include('layout.css')
    
</head>

<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
    <div class="wrapper">
        <!-- Navbar -->
        @include('layout.header')
        <!-- /.navbar -->
        
        <!-- Main Sidebar Container -->
        @include('layout.sidebar')
        <!-- Main Sidebar Container -->
        
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            @yield('content')
        </div>
        <!-- /.content-wrapper -->
        
        <!-- Main Footer -->
        {{-- @include('layout.footer') --}}
    </div>
    <!-- ./wrapper -->
    @include('layout.js')
</body>
</html>
