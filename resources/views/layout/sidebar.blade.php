<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/home')}}" class="brand-link">
        <img src="{{ asset('adminLte/dist/img/AdminLTELogo.png')}}" alt="Sai Matka Logo" class="brand-image img-circle elevation-3"
        style="opacity: .8">
        <span class="brand-text font-weight-light">{{ $company->name ?? 'Sai Matka' }}</span>
    </a>
    
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ asset('adminLte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="{{route('profile.edit')}}" class="d-block">{{Auth::user()->name}} <i class="ml-1 fa fa-spin fa-cog"></i> </a>
            </div>
        </div>
        
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                    <li class="nav-item">
                        <a href="{{ route('home')}}" class="nav-link">
                            <i class="nav-icon fas fa-th"></i>
                            <p> Dashboard </p>
                        </a>
                    </li>
                    @can('user.index')
                    <li class="nav-item">
                        <a href="{{ route('user.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-users"></i>
                            <p>User Management</p>
                        </a>
                    </li>
                    @endcan
                    @can('user.index')
                    <li class="nav-item">
                        <a href="{{ route('withdraw.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-rupee-sign"></i>
                            <p>Withdraw Management</p>
                        </a>
                    </li>
                    @endcan                    
                    @can('market.index')
                    <li class="nav-item">
                        <a href="{{ route('market.index')}}" class="nav-link">
                            <i class="nav-icon fas fa-trophy"></i>
                            <p>Market</p>
                        </a>
                    </li>
                    @endcan
                    @can('result.index')
                    <li class="nav-item">
                        <a href="{{ route('result.index')}}" class="nav-link">
                            <i class="nav-icon fa fa-gavel"></i>
                            <p>Result Management</p>
                        </a>
                    </li>
                    @endcan
                    @can('report.index')
                    <li class="nav-item">
                        <a href="{{ route('report.index')}}" class="nav-link">
                            <i class="nav-icon fa fa-th-list"></i>
                            <p>Report Management</p>
                        </a>
                    </li>
                    @endcan
                    @can('notification.index')
                    <li class="nav-item">
                        <a href="{{ route('notification.index')}}" class="nav-link">
                            <i class="nav-icon fa fa-bullhorn"></i>
                            <p>Send Notification</p>
                        </a>
                    </li>
                    @endcan
                </ul>
            </nav>
            <!-- /.sidebar-menu -->
        </div>
        <!-- /.sidebar -->
</aside>