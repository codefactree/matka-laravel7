<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src=" {{ asset('adminLte/plugins/jquery/jquery.min.js') }}"></script>
<!-- Bootstrap -->
<script src=" {{ asset('adminLte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src=" {{ asset('adminLte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src=" {{ asset('adminLte/dist/js/adminlte.js') }}"></script>

<!-- OPTIONAL SCRIPTS -->
<script src=" {{ asset('adminLte/dist/js/demo.js') }}"></script>

<!-- PAGE PLUGINS -->
{{-- <!-- jQuery Mapael -->
<script src=" {{ asset('adminLte/plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src=" {{ asset('adminLte/plugins/raphael/raphael.min.js') }}"></script>
<script src=" {{ asset('adminLte/plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
<script src=" {{ asset('adminLte/plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>

<!-- PAGE SCRIPTS -->
<script src=" {{ asset('adminLte/dist/js/pages/dashboard2.js') }}"></script> --}}

<script type="text/javascript">
    /**
    * Ajax Setup
    */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>
@stack('js')