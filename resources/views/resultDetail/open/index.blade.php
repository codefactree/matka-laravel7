@extends('layout.index')
@push('css')
<!-- SweetAlert2 -->
<link rel="stylesheet" href="{{asset('adminLte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Result</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Result</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    @include('helper.formError')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="row card-header">
                    <div class="col-md-4">
                        Date: <b>{{ convertYMDtoDMY(request()->get('date')) }}</b>
                    </div>
                    <div class="col-md-4 text-center">
                        <h3 class="card-title"> {{$market->name}} Open: Total = {{$totalAmount['total_bidding_amount']}} : Number = {{ $marketResult->first3.'-'.$marketResult->mid1  }}</h3>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-hover text-nowrap">
                        <thead>
                            <tr>
                                <th>Game</th>
                                <th>Rate</th>
                                <th>Total Amount</th>
                                <th>Giving Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <b> Single </b>
                                </td>
                                <td> {{ '10:'.$games[0]->rate }} </td>
                                <td> {{ $results['singleResults'] ? $results['singleResults']->total : 0}} </td>
                                <td> {{$results['singleResults'] ? floor($results['singleResults']->total * $games[0]->multiplication_factor) : 0 }} </td>
                                <td> 
                                    <div>
                                        @if($results['singleResults'])
                                        <a href="{{route('result.userDetail', ['market' => $market->id, 'game' => $games[0]->id, 'date' => request()->get('date') ])}}" class="btn btn-outline-success btn-sm"><i class="fas fa-eye"></i></a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b> Single Patti </b>
                                </td>
                                <td> {{ '10:'.$games[2]->rate }} </td>
                                <td> {{ $results['singlePattiResults'] ? $results['singlePattiResults']->total : 0}} </td>
                                <td> {{ $results['singlePattiResults'] ? floor($results['singlePattiResults']->total * $games[2]->multiplication_factor) : 0 }} </td>
                                <td> 
                                    <div>
                                        @if($results['singlePattiResults'])
                                        <a href="{{route('result.userDetail', ['market' => $market->id, 'game' =>  $games[2]->id, 'date' => request()->get('date')])}}" class="btn btn-outline-success btn-sm"><i class="fas fa-eye"></i></a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b> Double Patti </b>
                                </td>
                                <td> {{ '10:'.$games[3]->rate }} </td>
                                <td> {{$results['doublePattiResults'] ? $results['doublePattiResults']->total : 0}} </td>
                                <td> {{$results['doublePattiResults'] ? floor($results['doublePattiResults']->total * $games[3]->multiplication_factor) : 0}} </td>
                                <td> 
                                    <div>
                                        @if($results['doublePattiResults'])
                                        <a href="{{route('result.userDetail', ['market' => $market->id, 'game' => $games[3]->id, 'date' => request()->get('date')])}}" class="btn btn-outline-success btn-sm"><i class="fas fa-eye"></i></a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b> Tripple Patti </b>
                                </td>
                                <td> {{ '10:'.$games[4]->rate }} </td>
                                <td> {{ $results['tripplePattiResults'] ? $results['tripplePattiResults']->total: 0 }} </td>
                                <td> {{ $results['tripplePattiResults'] ? floor($results['tripplePattiResults']->total * $games[4]->multiplication_factor) : 0 }} </td>
                                <td> 
                                    <div>
                                        @if($results['tripplePattiResults'])
                                        <a href="{{route('result.userDetail', ['market' => $market->id, 'game' => $games[4]->id, 'date' => request()->get('date')])}}" class="btn btn-outline-success btn-sm"><i class="fas fa-eye"></i></a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            @can('forzeResult.sendMoney')
                <div class="text-center">
                    <form method="POST" action="{{route('forzeResult.sendMoney')}}">
                        @csrf
                        <input type="hidden" name="marketId" value="{{$market->id}}">
                        <input type="hidden" name="type" value="open">
                        <input type="hidden" name="date" value="{{request()->get('date') }}">
                        <button type="submit" class="btn btn-primary" {{$marketResult->is_open_result_generated ? "disabled" : null}}>Froze Result & Send Money</button>
                    </form>
                </div>
            @endcan
        </div>
    </div>
</section>

@endsection

@push('js')
<!-- SweetAlert2 -->
<script src="{{asset('adminLte/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<script src="{{asset('js/resultDetail.js')}}"></script>
@endpush