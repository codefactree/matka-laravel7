<table class="table table-hover text-nowrap">
    <thead>
        <tr>
            <th>Single</th>
            <th>Single Patti</th>
            <th>Double Patti</th>
            <th>Tripple Patti</th>
        </tr>
        <tr  class="bg-info">
            <th>Total : {{ $eachGameTotal[8]->total?? '-'}}</th>
            <th>Total : {{ $eachGameTotal[9]->total?? '-'}}</th>
            <th>Total : {{ $eachGameTotal[10]->total?? '-'}}</th>
            <th>Total : {{ $eachGameTotal[11]->total?? '-'}}</th>
        </tr>
    </thead>
    <tbody>
        @php
            $resultRowIndex = 1;
        @endphp
        @foreach ($results as $result)
            <tr>
                @for($gameIndex = 8; $gameIndex <= 11 ; $gameIndex ++)
                        @if (isset($results[$resultRowIndex][$gameIndex]))
                            <td>
                                <span>{{$results[$resultRowIndex][$gameIndex]->bidding_no .': '}}</span>
                                <span class="text-success"><b>{{$results[$resultRowIndex][$gameIndex]->total. 'Rs'}}</b></span>
                            </td>
                        @else
                            <td> - </td>
                        @endif
                @endfor
            </tr>
            @php
                $resultRowIndex++;
            @endphp
        @endforeach
    </tbody>
</table>