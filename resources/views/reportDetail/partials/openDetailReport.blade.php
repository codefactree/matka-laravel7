<table class="table table-hover text-nowrap">
    <thead>
        <tr>
            <th>Single</th>
            <th>Double</th>
            <th>Single Patti</th>
            <th>Double Patti</th>
            <th>Tripple Patti</th>
            <th>Half Sangam</th>
            <th>Full Sangam</th>
        </tr>
        <tr class="bg-info">
            <th> Total : {{ $eachGameTotal[1]->total?? '-'}}</th>
            <th>Total : {{ $eachGameTotal[2]->total ?? '-'}}</th>
            <th>Total : {{ $eachGameTotal[3]->total ?? '-'}}</th>
            <th>Total : {{ $eachGameTotal[4]->total ?? '-'}}</th>
            <th>Total : {{ $eachGameTotal[5]->total ?? '-'}}</th>
            <th>Total : {{ $eachGameTotal[6]->total ?? '-'}}</th>
            <th>Total : {{ $eachGameTotal[7]->total ?? '-'}}</th>
           
        </tr>
    </thead>
    <tbody>
        @php
            $resultRowIndex = 1;
        @endphp
        @foreach ($results as $result)
            <tr>
                @for($gameIndex = 1; $gameIndex < 8 ; $gameIndex ++)
                        @if (isset($results[$resultRowIndex][$gameIndex]))
                            <td>
                                <span>{{$results[$resultRowIndex][$gameIndex]->bidding_no .': '}}</span>
                                <span class="text-success"><b>{{$results[$resultRowIndex][$gameIndex]->total. 'Rs'}}</b></span>
                            </td>
                        @else
                            <td> - </td>
                        @endif
                @endfor
            </tr>
            @php
                $resultRowIndex++;
            @endphp
        @endforeach
    </tbody>
</table>