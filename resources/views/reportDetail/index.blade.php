@extends('layout.index')
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href=" {{ asset('adminLte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href=" {{ asset('adminLte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{asset('adminLte/plugins/daterangepicker/daterangepicker.css')}}">
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Report Detail</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/report')}}">Report</a></li>
                    <li class="breadcrumb-item active">ReportDetail</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content" id="capture">
    <div class="row">
        <div class="col-12">
            <div class="card" >
                <div class="card-header">
                    <h3 class="card-title"> <b>{{$market->name .' - '. request()->get('game-type').' : '. request()->get('date')}}</b></h3>
                    <input type="hidden" id="marketName" value="{{$market->name .'.'. request()->get('game-type').':'. request()->get('date')}}">
                    <button class="float-right btn btn-outline-primary ml-3" id="captureButton"><i class="fas fa-camera mr-1"></i>Capture</button>
                    <p class="float-right">Total Amount:<b> {{$totalAmount['total_bidding_amount']}} Rs</b></p>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    @if (request()->get('game-type') == 'open')
                        @include('reportDetail.partials.openDetailReport')
                    @endif
                    @if (request()->get('game-type') == 'close')
                        @include('reportDetail.partials.closeDetailReport')
                    @endif
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
</section>

@endsection

@push('js')
<!-- DataTables -->
<script src="{{ asset('adminLte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminLte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('adminLte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('adminLte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<!-- SweetAlert2 -->
<script src="{{asset('adminLte/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<!-- Data Range Picker  -->
<script src="{{ asset('adminLte/plugins/moment/moment.min.js')}}"></script>
<script src="{{ asset('adminLte/plugins/daterangepicker/daterangepicker.js')}}"></script>

<script src="{{asset('js/html2Canvas.min.js')}}"></script>
<script src="{{asset('js/report/reportDetail.js')}}"></script>
@endpush