<form method="POST" action="{{route('company.update')}}">
    @csrf
    @method('PUT')
    <div class="form-group row">
        <label for="name" class="col-sm-2 col-form-label">Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" value="{{$company->name}}" disabled>
        </div>
    </div>
    <div class="form-group row">
        <label for="owner_name" class="col-sm-2 col-form-label">Owner Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="owner_name" name="owner_name" value="{{$company->owner_name}}" placeholder="(Name to be displayed in app)">
        </div>
    </div>
    <div class="form-group row">
        <label for="mobile_no" class="col-sm-2 col-form-label">Mobile Number</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="mobile_no" name="mobile_no" value="{{$company->mobile_no}}" placeholder="(Mobile Number to be displayed in app)">
        </div>
    </div>
    <div class="text-center">
        <button type="submit" class="btn btn-success">Update Company Details</button>
    </div>
</form>