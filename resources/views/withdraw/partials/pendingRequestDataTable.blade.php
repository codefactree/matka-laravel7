<div class="card">
    <div class="card-header">
        <h3 class="card-title">Pending Withdraw Request List</h3>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
        <table id="withdrawDataTable" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Sr.No</th>
                    <th>Name</th>
                    <th>Requested Date</th>
                    <th>Amount</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /.card-body -->
</div>