@extends('layout.index')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Send Notification</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Send Notification</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Send Notification</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    @include('helper.formError')
                    <form role="form" class="form-horizontal" id="sendNotificationForm" action="{{route('notification.send')}}" method="POST">
                        @csrf
                        <div class="modal-body">
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="title" class="col-sm-2 col-form-label">Title<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title"  autofocus>
                                    </div>
                                </div>
                                
                                <div class="form-group row">
                                    <label for="name" class="col-sm-2 col-form-label">Body<span class="text-danger">*</span></label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" name="body" id="body" cols="30" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" id="saveUser" class="btn btn-primary"><i class="nav-icon fa fa-bullhorn"></i> Send</button>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</section>
@endsection

@push('js')
<!-- jquery-validation -->
<script src="{{asset('adminLte/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('adminLte/plugins/jquery-validation/additional-methods.min.js')}}"></script>
<script src="{{asset('js/sendNotification.js')}}"></script>
@endpush