<div id="errorDiv" class="alert alert-danger" style="display:none"></div>
@if (isset($errors) && count($errors))
<div class="alert alert-warning" role="alert">
    There were {{count($errors->all())}} Error(s)
    <ul>
        @foreach($errors->all() as $error)
        <li>{{ $error }} </li>
        @endforeach
    </ul>
</div>
@endif

@if(session('success'))
<div class="alert alert-success" role="alert">
    <h5>{{session('success')}}</h5>
</div>
@endif