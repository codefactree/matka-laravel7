<div align="center">
    <div style="font-style: italic; font-weight: bold;font-family:Helvetica;">
        
        <div style="font-size:20px;background-color:white; border-top: 5px inset blue; border-left: 5px inset blue; border-right: 5px inset blue; border-bottom: 5px inset blue; color:#000000; text-shadow: 1px 1px yellow;">
            
            @foreach($markets as $market)
            @php
            $marketResult = $market->result(getTodayDateYMD())->first();
            if(($marketResult)){
                $first3 = ($marketResult->first3) ?? '***'; 
                $mid1 = ($marketResult->mid1) ?? '*'; 
                $mid2 = ($marketResult->mid2) ?? '*'; 
                $last3 = ($marketResult->last3) ?? '***'; 
            } else {
                $timeDifference = \Carbon\Carbon::parse($market->open_time)->diffInMinutes(\Carbon\Carbon::now(), false);
                if($timeDifference < 0 && $timeDifference > -240) {
                    $first3 = '***';
                    $mid1 = '*';
                    $mid2 = '*';
                    $last3 = '***';
                } else {
                    $prevDate = date('Y-m-d', strtotime("-1 days"));
                    $marketYesterdatResult = $market->result($prevDate)->first();
                    if($marketYesterdatResult) {
                        $first3 = ($marketYesterdatResult->first3) ?? '***'; 
                        $mid1 = ($marketYesterdatResult->mid1) ?? '*'; 
                        $mid2 = ($marketYesterdatResult->mid2) ?? '*'; 
                        $last3 = ($marketYesterdatResult->last3) ?? '***'; 
                    } else {
                        $first3 = '***';
                        $mid1 = '*';
                        $mid2 = '*';
                        $last3 = '***';
                    }
                }
            }
            @endphp
            <div>{{$market->name}}</div>
            <div>{{$first3.'-'. $mid1. $mid2.'-'.$last3}}</div>
            <div>
                <span style="color:darkred; font-size:small;">
                    ({{$market->open_time.' - '. $market->close_time}})
                </span>
            </div>
            @endforeach
        </div>
    </div>
</div> 
