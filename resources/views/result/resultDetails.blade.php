@extends('layout.index')
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href=" {{ asset('adminLte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href=" {{ asset('adminLte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="{{asset('adminLte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">

<!-- daterange picker -->
<link rel="stylesheet" href="{{asset('adminLte/plugins/daterangepicker/daterangepicker.css')}}">
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Result Detail</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/result ')}}">Result</a></li>
                    <li class="breadcrumb-item active">Result Detail</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                <h2 id="marketName"class="card-title"> {{$market->name}} Result Management</h2>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <input type="hidden" id="marketId" value={{$market->id}}>
                    <table id="resultDetailDataTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Sr.No</th>
                                <th>Date</th>
                                <th>Number</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</section>
@endsection

@push('js')
<!-- DataTables -->
<script src="{{ asset('adminLte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminLte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('adminLte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('adminLte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<!-- InputMask -->
<script src="{{ asset('adminLte/plugins/moment/moment.min.js')}}"></script>
<!-- date-range-picker -->

<!-- SweetAlert2 -->
<script src="{{asset('adminLte/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<script src="{{asset('js/resultDetail.js')}}"></script>
@endpush