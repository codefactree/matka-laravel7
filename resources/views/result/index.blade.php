@extends('layout.index')
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href=" {{ asset('adminLte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href=" {{ asset('adminLte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="{{asset('adminLte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">

<!-- daterange picker -->
<link rel="stylesheet" href="{{asset('adminLte/plugins/daterangepicker/daterangepicker.css')}}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('eonasdanDatePicker/css/bootstrap-datetimepicker.min.css')}}"> 

<!-- Select2 -->
<link rel="stylesheet" href="{{asset('adminLte/plugins/select2/css/select2.min.css')}}">
<link rel="stylesheet" href="{{asset('adminLte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Result</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Result</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    @include('helper.formError')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="col-md-4 card-title">Result Management</h3>
                    <div class="col-md-3 float-right">
                        <div class="input-group">
                            <label for="name" class="col-form-label mr-2">Date: </label>
                            <input type="text" class="form-control" id="resultDate" name="resultDate">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="far fa-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="resultDataTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Sr.No</th>
                                <th>Name</th>
                                <th>Open Time</th>
                                <th>Action</th>
                                <th>Close Time</th>
                                <th>Action</th>
                                <th>Details</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</section>
@include('result.open.view') 
@include('result.open.edit')
@include('result.close.view') 
@include('result.close.edit')
@endsection

@push('js')
<!-- DataTables -->
<script src="{{ asset('adminLte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminLte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('adminLte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('adminLte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<!-- InputMask -->
<script src="{{ asset('adminLte/plugins/moment/moment.min.js')}}"></script>
<!-- date-range-picker -->


<!-- jquery-validation -->
<script src="{{asset('adminLte/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('adminLte/plugins/jquery-validation/additional-methods.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{asset('adminLte/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('eonasdanDatePicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<!-- date-range-picker -->
<script src="{{asset('adminLte/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Select2 -->
<script src="{{asset('adminLte/plugins/select2/js/select2.full.min.js')}}"></script>
<script src="{{asset('js/result.js')}}"></script>
@endpush