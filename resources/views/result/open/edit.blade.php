<!-- Modal -->
<div class="modal fade" id="openActionModal" tabindex="-1" role="dialog" aria-labelledby="openActionModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="openActionModalLabel">Open Time</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="errorDiv" class="alert alert-danger" style="display:none"></div>
            <form role="form" class="form-horizontal" id="openActionForm" >
                <div class="modal-body">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Market</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="nameOpen" name="nameOpen" disabled>
                                <input type="hidden" class="form-control" id="marketIdOpen" name="marketIdOpen">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Open Time</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id='open_time' name='open_time' disabled>
                                        <input type="hidden" class="form-control" id='open_timeHidden' name='open_timeHidden'>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="far fa-clock"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Date</label>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="dateOpen" name="dateOpen">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="far fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Open Panna</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="first3" name="first3" autofocus>
                                {{-- <select class="form-control select2" style="width: 100%;" id="first3" name="first3">
                                    <option value="">Enter Result</option>
                                    @foreach (allResultValues() as $resultValues)
                                        <option value="{{$resultValues}}">{{$resultValues}}</option>
                                    @endforeach
                                </select> --}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Open Single</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fourth_number" name="fourth_number" disabled>
                                <input type="hidden" class="form-control" id="mid1" name="mid1" >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="saveOpenResult" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>