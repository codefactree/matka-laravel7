<!-- Modal -->
<div class="modal fade" id="viewOpenActionModal" tabindex="-1" role="dialog" aria-labelledby="viewOpenActionModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="openActionModalLabel">Open Time</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="errorDiv" class="alert alert-danger" style="display:none"></div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Market</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nameOpenView" name="nameOpenView" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Open Time</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" id='open_timeView' name='open_timeView' disabled>
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="far fa-clock"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Date</label>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" id="dateOpenView" name="dateOpenView" disabled>
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="far fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Open Panna</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="first3View" name="first3View" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Open Single</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="fourth_numberView" name="fourth_numberView" disabled>
                            <input type="hidden" class="form-control" id="mid1View" name="mid1View" >
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</div>