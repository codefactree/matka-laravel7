<!-- Modal -->
<div class="modal fade" id="viewCloseActionModal" tabindex="-1" role="dialog" aria-labelledby="viewCloseActionModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="closeActionModalLabel">Close Time</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="errorDiv" class="alert alert-danger" style="display:none"></div>
            <div class="modal-body">
                <div class="card-body">
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Market</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nameCloseView" name="nameCloseView" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Close Time</label>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" id='close_timeView' name='close_timeView' disabled>
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="far fa-clock"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Date</label>
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" class="form-control" id="dateCloseView" name="dateCloseView" disabled>
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="far fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Close Panna</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control" id="last3View" name="last3View" disabled>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="name" class="col-sm-2 col-form-label">Close Single</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="fifth_numberView" name="fifth_numberView" disabled>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" id="closeModalView" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</div>