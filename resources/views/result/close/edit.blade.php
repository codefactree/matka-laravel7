<!-- Modal -->
<div class="modal fade" id="closeActionModal" tabindex="-1" role="dialog" aria-labelledby="closeActionModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="closeActionModalLabel">Close Time</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="errorDiv" class="alert alert-danger" style="display:none"></div>
            <form role="form" class="form-horizontal" id="closeActionForm" >
                <div class="modal-body">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Market</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="nameClose" name="nameClose" disabled>
                                <input type="hidden" class="form-control" id="marketIdClose" name="marketIdClose">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Close Time</label>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id='close_time' name='close_time' disabled>
                                        <input type="hidden" class="form-control" id='close_timeHidden' name='close_timeHidden'>
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="far fa-clock"></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Date</label>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="dateClose" name="dateClose">
                                    <div class="input-group-append">
                                        <span class="input-group-text"><i class="far fa-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Close Panna</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="last3" name="last3" autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Close Single</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="fifth_number" name="fifth_number" disabled>
                                <input type="hidden" class="form-control" id="mid2" name="mid2" >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="closeModalEdit" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="saveCloseResult" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>