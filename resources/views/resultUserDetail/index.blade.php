@extends('layout.index')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Result User Details</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/result')}}">Result</a></li>
                    <li class="breadcrumb-item active">Result Detail</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    @include('helper.formError')
    @php
       $selectedGameId = -- $selectedGame->id; // as array start from 0
    @endphp
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="row card-header">
                    <div class="col-md-4">
                        Date: <b>{{ convertYMDtoDMY(request()->get('date')) }}</b>
                    </div>
                    <div class="col-md-8 text-center">
                        <h3 class="card-title"> 
                            <span>{{$market->name .' '.$selectedGame->type}} ||</span>
                            <span>Bid Amount = {{$resultTotalDetail->total_amount}} ||</span>
                            <span>Giving Amount = {{$resultTotalDetail->total_amount * $games[$selectedGameId]->multiplication_factor}}</span>
                        </h3>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table class="table table-hover text-nowrap">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Mobile Number</th>
                                <th>Bidding Number</th>
                                <th>Bidding Amount</th>
                                <th>Giving Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($resultUserDetails as $userDetail)
                                <tr>
                                    <td> {{ $userDetail->name}} </td>
                                    <td> {{ $userDetail->email}} </td>
                                    <td> {{ $userDetail->mobile_no}} </td>
                                    <td> {{ $userDetail->bidding_no}} </td>
                                    <td> {{ $userDetail->amount}} </td>
                                    <td> {{ $userDetail->amount * $games[$selectedGameId]->multiplication_factor }} </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</section>

@endsection
@push('js')
<!-- SweetAlert2 -->
<script src="{{asset('adminLte/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<script src="{{asset('js/resultDetail.js')}}"></script>
@endpush