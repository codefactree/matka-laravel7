@extends('layout.index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>View User</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/user')}}">User</a></li>
                    <li class="breadcrumb-item active">View</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$user->name}}</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div id="errorDiv" class="alert alert-danger" style="display:none"></div>
                    <form role="form" class="form-horizontal" id="userForm" >
                        <div class="modal-body">
                            <div class="card-body">
                                @include('user.partials.view')
                                @include('userDetail.partials.view')
                            </div>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</section>
@endsection