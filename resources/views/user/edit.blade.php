@extends('layout.index')
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Edit User</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/user')}}">User</a></li>
                    <li class="breadcrumb-item active">Edit</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$user->name}}</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    @include('helper.formError')
                    <form role="form" class="form-horizontal" id="userUpdateForm" method="POST" action="{{ route('user.update', $user->id) }}">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="card-body">
                                @include('user.partials.edit')
                                @include('userDetail.partials.edit')
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" id="updateUser" class="btn btn-primary">Update</button>
                        </div>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</section>
@endsection

@push('js')
<!-- jquery-validation -->
<script src="{{asset('adminLte/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('adminLte/plugins/jquery-validation/additional-methods.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{asset('adminLte/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('js/user/edit.js')}}"></script>
@endpush