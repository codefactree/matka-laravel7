<div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Role<span class="text-danger">*</span></label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name of User" value="{{$user->role->name}}" disabled>
    </div>
</div>
<div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Name<span class="text-danger">*</span></label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name of User" value="{{$user->name}}" disabled>
    </div>
</div>
<div class="form-group row">
    <label for="email" class="col-sm-2 col-form-label">Email<span class="text-danger">*</span></label>
    <div class="col-sm-10">
        <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}"placeholder="Enter Email of User" disabled>
    </div>
</div>
<div class="form-group row">
    <label for="user_name" class="col-sm-2 col-form-label">User Name<span class="text-danger">*</span></label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="user_name" name="user_name" value="{{$user->user_name}}" placeholder="Enter User Name of User" disabled>
    </div>
</div>
<div class="form-group row">
    <label for="mobile_no" class="col-sm-2 col-form-label">Mobile Number<span class="text-danger">*</span></label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="mobile_no" name="mobile_no" value="{{$user->mobile_no}}" placeholder="Enter Mobile Number of User" disabled>
    </div>
</div>
<div class="form-group row">
    <label for="location" class="col-sm-2 col-form-label">Location</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="location" name="location" value="{{$user->location}}" placeholder="Enter Location of User" disabled>
    </div>
</div>
<div class="form-group row">
    <label for="wallet_amount" class="col-sm-2 col-form-label">Wallet Amount</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="wallet_amount" name="wallet_amount" value="{{$user->wallet_amount}}"  placeholder="Enter Wallet Amount for User" disabled>
    </div>
</div>