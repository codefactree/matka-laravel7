<!-- Modal -->
<div class="modal fade" id="walletModal" tabindex="-1" role="dialog" aria-labelledby="walletModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="walletModalLabel">Add Money</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="errorDiv" class="alert alert-danger" style="display:none"></div>
            <form role="form" class="form-horizontal" id="walletEditForm">
                @csrf
                <input type="hidden" id="id" name="id">
                <div class="modal-body">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-sm-5 col-form-label">Amount</label>
                            <div class="col-sm-7">
                                <input type="number" class="form-control" id="amount" name="amount" placeholder="Enter Amount" autofocus>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="closeModalWallet" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="updateWallet" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>