<div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Role<span class="text-danger">*</span></label>
    <div class="col-sm-10">
        <select id='role' name='role' class="form-control">
            @foreach ($roles as $role)
            <option value="{{$role->id}}" {{$role->id == 2 ? 'selected' : ''}}>{{$role->name}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group row">
    <label for="name" class="col-sm-2 col-form-label">Name<span class="text-danger">*</span></label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name of User" value="{{old('name')}}" autofocus>
    </div>
</div>
<div class="form-group row">
    <label for="email" class="col-sm-2 col-form-label">Email<span class="text-danger">*</span></label>
    <div class="col-sm-10">
        <input type="email" class="form-control" id="email" name="email" value="{{old('email')}}"placeholder="Enter Email of User">
    </div>
</div>
<div class="form-group row">
    <label for="user_name" class="col-sm-2 col-form-label">User Name<span class="text-danger">*</span></label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="user_name" name="user_name" value="{{old('user_name')}}" placeholder="Enter User Name of User">
    </div>
</div>
<div class="form-group row">
    <label for="password" class="col-sm-2 col-form-label">Password<span class="text-danger">*</span></label>
    <div class="col-sm-10">
        <input type="password" class="form-control" id="password" name="password" value="{{old('password')}}" placeholder="Enter Password">
    </div>
</div>
<div class="form-group row">
    <label for="mobile_no" class="col-sm-2 col-form-label">Mobile Number<span class="text-danger">*</span></label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="mobile_no" name="mobile_no" value="{{old('mobile_no')}}" placeholder="Enter Mobile Number of User">
    </div>
</div>
<div class="form-group row">
    <label for="location" class="col-sm-2 col-form-label">Location</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="location" name="location" value="{{old('location')}}" placeholder="Enter Location of User">
    </div>
</div>
<div class="form-group row">
    <label for="wallet_amount" class="col-sm-2 col-form-label">Wallet Amount</label>
    <div class="col-sm-10">
        <input type="number" class="form-control" id="wallet_amount" name="wallet_amount" value="{{old('wallet_amount', 0)}}"  placeholder="Enter Wallet Amount for User">
    </div>
</div>