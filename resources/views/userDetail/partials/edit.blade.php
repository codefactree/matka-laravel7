<div class="form-group row">
    <label for="phonePe" class="col-sm-2 col-form-label">Phone Pe</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="phonePe" name="phonePe" 
        value="{{$user->userDetail->phonePe ?? ''}}"
        placeholder="Enter Phone Pe number of User">
    </div>
</div>
<div class="form-group row">
    <label for="googlepay" class="col-sm-2 col-form-label">Google Pay</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="googlepay" name="googlepay" 
        value="{{$user->userDetail->googlepay ?? ''}}"
        placeholder="Enter Google Pay number of User">
    </div>
</div>
<div class="form-group row">
    <label for="paytm" class="col-sm-2 col-form-label">PayTm</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="paytm" name="paytm" 
        value="{{$user->userDetail->paytm ?? ''}}"
        placeholder="Enter User Name of User">
    </div>
</div>
<div class="form-group row">
    <label for="bank_name" class="col-sm-2 col-form-label">Bank Name</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="bank_name" name="bank_name" 
        value="{{$user->userDetail->bank_name ?? ''}}"
        placeholder="Enter Bank Name">
    </div>
</div>
<div class="form-group row">
    <label for="branch_name" class="col-sm-2 col-form-label">Branch Name</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="branch_name" name="branch_name" 
        value="{{$user->userDetail->branch_name ?? ''}}"
        placeholder="Enter Branch Name of Bank">
    </div>
</div>
<div class="form-group row">
    <label for="account_no" class="col-sm-2 col-form-label">Account No</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="account_no" name="account_no" 
        value="{{$user->userDetail->account_no ?? ''}}"
         placeholder="Enter Account Number of Bank">
    </div>
</div>
<div class="form-group row">
    <label for="ifsc_code" class="col-sm-2 col-form-label">IFSC</label>
    <div class="col-sm-10">
        <input type="text" class="form-control" id="ifsc_code" name="ifsc_code" 
        value="{{$user->userDetail->ifsc_code ?? ''}}"
        placeholder="Enter IFSC code for Bank">
    </div>
</div>