<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="createModalLabel">Create Market</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="errorDiv" class="alert alert-danger" style="display:none"></div>
            <form role="form" class="form-horizontal" id="marketForm" >
                <div class="modal-body">
                    <div class="card-body">
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Enter Name of Market" value="{{old('name')}}" autofocus>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="row form-group">
                                    <label for="open_time" class="col-sm-4 col-form-label">Open Time</label>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id='openTimePicker' name='openTimePicker'>
                                                <div class="input-group-append">
                                                <span class="input-group-text"><i class="far fa-clock"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6">
                                <div class="row form-group">
                                    <label for="open_time" class="col-sm-4 col-form-label">Close Time</label>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <input type="text" class="form-control" id='closeTimePicker' name='closeTimePicker'>
                                                <div class="input-group-append">
                                                <span class="input-group-text"><i class="far fa-clock"></i></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="week" class="col-sm-2 col-form-label">Week</label>
                            <div class="col-sm-10">
                                @foreach (config('constant.week') as $item)
                                    <div class="form-check">
                                        <input type="checkbox" class="form-check-input" name="days[]" id="{{$item}}" value="{{$item}}">
                                        <label class="form-check-label" for="{{$item}}">{{$item}}</label>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="closeModal" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" id="saveMarket" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>
</div>