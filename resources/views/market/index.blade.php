@extends('layout.index')
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href=" {{ asset('adminLte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href=" {{ asset('adminLte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="{{asset('adminLte/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">

<!-- daterange picker -->
<link rel="stylesheet" href="{{ asset('eonasdanDatePicker/css/bootstrap-datetimepicker.min.css')}}"> 
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Market</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Market</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Market List</h3>
                    @can('market.create')
                        <button class="btn btn-outline-info btn-sm float-right" data-toggle="modal" data-target="#createModal"><i class="fas fa-plus pr-1"></i>Create Market</button>
                        <a href="{{route('market.reorder')}}" class="btn btn-outline-success btn-sm float-right  mr-2" ><i class="fas fa-retweet pr-1"></i>Re-Order Market</a>
                    @endcan
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="marketDataTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Sr.No</th>
                                <th>Name</th>
                                <th>Open Time</th>
                                <th>Close Time</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</section>
@can('market.show')
    @include('market.view') 
@endcan
@can('market.create')
    @include('market.create')
@endcan
@can('market.edit')
    @include('market.edit')
@endcan
@endsection

@push('js')
<!-- DataTables -->
<script src="{{ asset('adminLte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminLte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('adminLte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('adminLte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
<!-- InputMask -->
<script src="{{ asset('adminLte/plugins/moment/moment.min.js')}}"></script>
<!-- date-range-picker -->


<!-- jquery-validation -->
<script src="{{asset('adminLte/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
<script src="{{asset('adminLte/plugins/jquery-validation/additional-methods.min.js')}}"></script>
<!-- SweetAlert2 -->
<script src="{{asset('adminLte/plugins/sweetalert2/sweetalert2.min.js')}}"></script>
<script src="{{asset('eonasdanDatePicker/js/bootstrap-datetimepicker.min.js')}}"></script>
<script src="{{asset('js/market.js')}}"></script>
@endpush