@extends('layout.index')

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Market</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('/market')}}">Market</a></li>
                    <li class="breadcrumb-item active">Reorder</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Market Order</h3>
                </div>
                <!-- /.card-header -->
                @include('helper.formError')
                <div class="card-body">
                    <div>
                        <div class="row">
                            <label class="col-md-4"> Market Name</label>
                            <label class="col-md-1"> Current Sort Order</label>
                            <label class="col-md-1 ml-5"> New Sort Order</label>
                        </div>
                        <form action="{{route('reorder.update')}}" method="POST">
                            @csrf
                            @method('PUT')
                            @foreach ($markets as $market)
                                <div class="row">
                                    <label class="col-md-4"> {{$market->name}} </label>
                                    <input class="form-group col-md-1" type="text" value={{$market->sort_order}} disabled>
                                    <input class="form-group col-md-1 ml-5" type="number" name="{{$market->id}}" value={{$market->sort_order}}>
                                </div>
                            @endforeach

                            <div class="row justify-content-center">
                                <button type="submit" class="btn btn-success"> Save</button>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</section>
@endsection
