@extends('layout.index')
@push('css')
<!-- DataTables -->
<link rel="stylesheet" href=" {{ asset('adminLte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
<link rel="stylesheet" href=" {{ asset('adminLte/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
<!-- daterange picker -->
<link rel="stylesheet" href="{{asset('adminLte/plugins/daterangepicker/daterangepicker.css')}}">
@endpush
@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1>Report Generation</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="{{url('/home')}}">Home</a></li>
                    <li class="breadcrumb-item active">Report</li>
                </ol>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
    @include('helper.formError')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Report For Total Day</h3>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <div class="form-group col-sm-4">
                            <div class="input-group">
                                <label for="name" class="col-form-label">Total Amount: </label>
                                <input type="text" class="form-control" disabled id="total_amount" name="total_amount">
                            </div>
                        </div>
                        <div class="form-group col-sm-4 ml-4">
                            <div class="input-group">
                                <label for="name" class="col-form-label">Date: </label>
                                <input type="text" class="form-control" id="reportDate" name="reportDate">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="far fa-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Market List</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="reportDataTable" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th>Sr.No</th>
                                <th>Market</th>
                                <th>Type </th>
                                <th>Amount</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
</section>

@endsection

@push('js')
<!-- DataTables -->
<script src="{{ asset('adminLte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminLte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('adminLte/plugins/datatables-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('adminLte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>

<!-- SweetAlert2 -->
<script src="{{asset('adminLte/plugins/sweetalert2/sweetalert2.min.js')}}"></script>

<!-- Data Range Picker  -->
<script src="{{ asset('adminLte/plugins/moment/moment.min.js')}}"></script>
<script src="{{ asset('adminLte/plugins/daterangepicker/daterangepicker.js')}}"></script>

<script src="{{asset('js/report/report.js')}}"></script>
@endpush