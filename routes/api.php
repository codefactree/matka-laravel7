<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('API')->group(function () {
    // Controllers Within The "App\Http\Controllers\API" Namespace
    Route::post('login','Auth\LoginController@login');
    Route::post('new_register/sendOtp','Auth\RegisterController@sendOtp');
    Route::post('new_register/validateOtp','Auth\RegisterController@validateOtp');
    Route::post('new_register/submit','Auth\RegisterController@register');

    Route::post('forget/sendOtp','Auth\ForgetController@sendOtp');
    Route::post('forget/validateOtp','Auth\ForgetController@validateOtp');
    Route::post('forget/reset','Auth\ForgetController@reset');
    Route::get('company','UserController@getCompany');

    Route::middleware(['auth:api'])->group(function () {
        Route::get('dashboard','DashboardController@getDashboardDetails');
        Route::get('profile','UserController@getUserDetails');
        Route::post('profile','UserController@update');
        Route::get('account-statement','UserController@getAccountStatement');
        Route::post('withdraw/request','WithdrawRequestController@storeWithdrawRequest');
        Route::get('getAllGames','GameController@getAllGames');
        Route::post('submit/bid','GameController@submitBid');
        Route::get('result-values','GameController@allResultValues');
        Route::post('logout','Auth\LogoutController@logout');
        Route::post('token','PushNotificationController@saveToken');
    });
});