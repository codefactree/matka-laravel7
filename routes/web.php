<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('admin')->group(function () {
Route::group(['middleware' => ['auth', 'role:Admin']], function() {
    
    Route::resource('user','UserController');
    Route::get('getAllUsers','UserController@getAllUsers');
    Route::get('/user/{user}/account-statement','UserController@accountStatement')->name('user.accountStatement');
    Route::get('/getAccountStatement/{user}','UserController@getAccountStatement');
    
    Route::resource('market','MarketController')->only([
        'index', 'store', 'show', 'update', 'destroy'
        ]);
        Route::get('getAllMarkets','MarketController@getAllMarkets');
        Route::get('reorder','MarketController@reorder')->name('market.reorder');
        Route::put('reorder','MarketController@updateReorder')->name('reorder.update');
        
        Route::put('wallet/addMoney/{user}', 'UserController@addMoneyToWallet');
        
        Route::get('getPendingWithdrawRequest','WithdrawRequestController@getPendingWithdrawRequest');
        Route::put('wallet/withDraw/changeStatus/{id}', 'WithdrawRequestController@changeWithdrawStatus');
        Route::get('/withdraw','WithdrawRequestController@index')->name('withdraw.index');
        Route::get('/getWithdrawList','WithdrawRequestController@getWithdrawList');

        Route::get('profile','ProfileController@edit')->name('profile.edit');
        Route::put('profile','ProfileController@update')->name('profile.update');
        
        Route::put('company','CompanyController@update')->name('company.update');
        
        Route::get('report','ReportController@index')->name('report.index');
        Route::get('getReport/{date}','ReportController@getReport');
        Route::get('report/total-bid/{date}','ReportController@getTotalBidAmount');
        Route::get('report/market/{market}','ReportDetailController@index')->name('reportDetail.index');
        
        Route::get('send-notification','PushNotificationController@index')->name('notification.index');
        Route::post('send-notification','PushNotificationController@send')->name('notification.send');
    });
    
    Route::group(['middleware' => ['role:Employee|Admin']], function() {
        Route::get('/home', 'HomeController@index')->name('home');
        
        Route::get('result','ResultController@index')->name('result.index');
        Route::get('result/market/{market}','ResultController@resultForMarket')->name('result.resultForMarket');
        Route::post('result/open','ResultController@storeOpenResult')->name('result.storeOpenResult');
        Route::post('result/close','ResultController@storeCloseResult')->name('result.storeCloseResult');
        Route::put('result/delete/open/{market}','ResultController@deleteOpenResult')->name('result.deleteOpenResult');
        Route::put('result/delete/close/{market}','ResultController@deleteCloseResult');
        Route::get('result/generate/open/{market}','ResultController@generateOverallOpenResult')->name('result.generateOpenResult');
        Route::get('result/generate/close/{market}','ResultController@generateOverallCloseResult')->name('result.generateCloseResult');
        Route::get('result/generate/userDetail/{market}/{game}','ResultController@resultUserDetail')->name('result.userDetail');
        
        Route::post('result/frozeResult/transferMoney','ResultController@frozeResultAndTransferMoney')->name('forzeResult.sendMoney');
        Route::get('getAllResult/{date}','ResultController@getAllResult');
        Route::get('result/details/{market}','ResultController@resultDetails')->name('result.details');
        Route::get('result/getDetails/{market}','ResultController@resultDetailsForMarket')->name('result.getDetails');
    });

    Route::get('/', function () {
        return view('auth.login');
    })->middleware('guest')->name('login');
    
    Auth::routes();
});

Route::get('/', 'FrontendController@index');
Route::get('terms', 'FrontendController@terms');
Route::get('privacypolicy', 'FrontendController@privacypolicy');